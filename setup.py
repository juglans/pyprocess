MIN_PY_VER = '3.6'
DISTNAME = 'pyprocess'
DESCRIPTION = 'Preprocessing scripts from the Seeley lab at UCSF.'
LICENSE = 'BSD 3-Clause'
DOWNLOAD_URL = 'https://gitlab.com/pyprocess/pyprocess'

CLASSIFIERS = [
    'Development Status :: 4 - Alpha',
    'Intended Audience :: Science/Research',
    'License :: OSI Approved :: BSD License',
    'Programming Language :: Python :: 3 :: Only',
    'Programming Lanuage :: Python :: 3.6',
    'Programming Language :: Python :: 3.7',
    'Programming Language :: Python :: 3.8',
    'Operating System :: Unix',
    ]

import os
import sys
from setuptools import setup, find_packages
import versioneer

if sys.version_info < (3, 6):
    current_python_ver = '.'.join(str(v) for v in sys.version_info[:3])
    sys.stderr.write(
            f'You are using Python {current_python_ver}.\n' +
             'Currently pyprocess only supports Python 3.6 and above.\n' +
             'Please install Python 3.6 to continue.\n')
    sys.exit(1)

PACKAGES = [package for package in find_packages() if True]

if __name__ == '__main__':
    setup(
            name='pyprocess',
            version=versioneer.get_version(),
            cmdclass=versioneer.get_cmdclass(),
            setup_requires=[],
            )
