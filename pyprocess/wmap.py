import re
import pathlib
import numpy as np
import nibabel as nib
import argparse
import filehandling
import nipypeutils
from nipype import pipeline
from nipype.interfaces import utility
import json
from typing import Union, Tuple, List
import sys
import pandas as pd
import shutil

FILE_DIRECTORY = pathlib.Path(__file__).resolve()
WMAP_JSON_MAPPER = FILE_DIRECTORY.parent / "wmap/mapper.json"
INPUT_HELPSTRING = """
Fullpath to a CSV file containing columns:
full-path-to-smwc1-file, demog_var_1, demog_var_2, ..., demog_var_last

Note that the order of the demographics variables MUST be:
Model without education {pass --model WithoutEducation}: 
    1. Age (years)
    2. Sex [1/2 M/F]
    3. Hand [1/2 R/(L or ambidextrous)]
    4. TIV [calculated from SPM]
    5. 0/1 Indicator for old SFVA scanner (1.5T)
    6. 0/1 Indicator for Trio scanner (3T)
    7. 0/1 Indicator for Prisma scanner (3T)
    8. 0/1 Indicator for SFVA scanner (4T)
Model with education (pass --model WithEducation):
    Same as above but with:
    9: Education (years)

You can provide a CSV without a header as well, but the order of the columns must be the same.
Paths to nifti files can be nii or nii.gz
"""


def read_json(json_file: Union[str, pathlib.Path]):
    with open(json_file, "r") as f:
        json_obj = json.load(f)

    return json_obj


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        "input_file",
        help=INPUT_HELPSTRING,
        type=str,
    )

    parser.add_argument(
        "output_directory",
        help="Fullpath of desired output data directory.",
        type=str,
    )

    parser.add_argument(
        "--useeduc",
        help="Use education as covariate in the model. Default False.",
        type=bool,
        default=False,
    )

    parser.add_argument(
        "--brainmask", help="Path to brainmask that you want to use.", default=False
    )

    parser.add_argument(
        "--model",
        help="String that describes the type of model; see mapper.json file for available choices.",
        type=str,
        default="WithoutEducation",
    )

    parser.add_argument(
        "--remove_tmpfiles",
        help="Remove temporary files created by nipype after run.",
        action="store_true",
    )
    resource_opts = parser.add_argument_group(
        "resources",
        "Flags and options to control computing resources such as number of cores and amount of memory.",
    )

    resource_opts.add_argument(
        "--n_procs", default=4, type=int, help="Number of processes to use."
    )
    resource_opts.add_argument(
        "--memory_gb",
        default=12,
        type=int,
        help="Amount of memory in GB to use in total.",
    )

    args = parser.parse_args()

    args.output_directory = pathlib.Path(args.output_directory)
    args.input_file = pathlib.Path(args.input_file)
    if args.brainmask not in [None, False]:
        args.brainmask = pathlib.Path(args.brainmask).resolve()

    model_json = read_json(WMAP_JSON_MAPPER)
    keys = model_json.keys()
    if args.model not in model_json.keys():
        print(
            f"Key provided: {args.model} was not a potential option for currently used wmap models. Available keys:"
        )
        for key in keys:
            print("\t", key)

        print(
            "Please select one of these models and run the script with --model {KEYNAME}"
        )
        sys.exit(1)
    else:
        args.model_dict = validate_model_dict(model_json[args.model])

    if args.output_directory.exists() is False:
        args.output_directory.mkdir(exist_ok=True, parents=True)
    return args


def create_wmap_analysis(
    input_file: str,
    scan_name: str,
    session_name: str,
    output_directory: str,
    demvars: list,
    model_dict: dict,
    brainmask: bool = False,
):
    from nibabel import load, Nifti1Image
    from numpy import zeros_like
    from os.path import join as opjoin
    from os import makedirs
    from json import dump

    subject_anat_dir = opjoin(output_directory, scan_name, session_name, "anat")
    makedirs(subject_anat_dir, exist_ok=True)

    input_nib = load(input_file)
    data = input_nib.get_fdata()

    prediction = zeros_like(data)
    for idx, demvar in enumerate(demvars):
        betamap = load(model_dict[str(idx + 1)]).get_fdata()
        prediction += betamap * demvar

    intercept_map = load(model_dict["intercept"]).get_fdata()
    prediction += intercept_map

    residual_map = load(model_dict["sdresiduals"]).get_fdata()
    residuals = (data - prediction) / residual_map
    residuals *= -1

    if brainmask:
        bmask = load(brainmask)
        bmask_data = bmask.get_fdata()
        residuals *= bmask_data

    output_filename = f"{scan_name}_{session_name}_desc-wmap.nii.gz"
    output_filepath = opjoin(subject_anat_dir, output_filename)
    residual_img = Nifti1Image(residuals, input_nib.affine, header=input_nib.header)
    residual_img.to_filename(output_filepath)

    record_dict = {}
    record_dict["demvars_used"] = [
        str(el) for el in demvars
    ]  # need to convert elements to string to make json happy
    record_dict["covariates"] = model_dict

    json_filename = f"{scan_name}_{session_name}_desc-wmapdesc.json"
    json_output_filepath = opjoin(subject_anat_dir, json_filename)
    with open(json_output_filepath, "w") as f:
        dump(record_dict, f, indent=4)

    return output_filepath, record_dict


def wmap_workflow(
    df: pd.DataFrame,
    output_directory: Union[str, pathlib.Path],
    model_dict: dict,
    n_procs: int = 1,
    memory_gb: int = 10,
    brainmask: bool = False,
):

    workflow, tmpdir = nipypeutils.get_main_workflow()

    for _, row in df.iterrows():
        scan_path = row.iloc[0]
        demographics_vars = row.iloc[1:]
        scan_search = re.search("(sub-\w+)_.*", scan_path)
        session_search = re.search("(ses-\d+)", scan_path)
        mistake = False

        if scan_search is None:
            mistake = True
            print(
                f"Scan {scan_search}; could not find PIDN. Please add a subject string with sub-**** to the filename so that we can index it and save it properly."
            )

        if session_search is None:
            mistake = True
            print(
                f"Scan {scan_name}; could not find session. Please add a subject string with ses-**** to the filename so that we can index it and save it properly."
            )

        if mistake:
            sys.exit(1)

        subject_name = scan_search.group(1)
        session_name = session_search.group(1)

        wmap_node = pipeline.Node(
            interface=utility.Function(
                function=create_wmap_analysis,
                input_names=[
                    "input_file",
                    "scan_name",
                    "session_name",
                    "output_directory",
                    "demvars",
                    "model_dict",
                    "brainmask",
                ],
                output_names=["out_file", "history"],
            ),
            name=f"{subject_name}_{session_name}_wmap",
        )

        wmap_node.inputs.input_file = scan_path
        wmap_node.inputs.scan_name = subject_name
        wmap_node.inputs.session_name = session_name
        wmap_node.inputs.output_directory = output_directory
        wmap_node.inputs.demvars = demographics_vars
        wmap_node.inputs.model_dict = model_dict
        wmap_node.inputs.brainmask = brainmask

        print(f"Finished workflow setup for: {subject_name}, {session_name}.")

        workflow.add_nodes([wmap_node])

    workflow.run(
        plugin="MultiProc",
        plugin_args={
            "n_procs": n_procs,
            "memory_gb": memory_gb,
            "crashfile_format": "txt",
        },
    )
    return tmpdir


def validate_model_dict(modeldict: dict):
    failed = False
    wmap_dir = FILE_DIRECTORY.parent / "wmap"
    for key, val in modeldict.items():
        pth = wmap_dir / val
        if pth.exists() is False:
            failed = True
            print(
                f"Model file: {pth} could not be found. Please check that it exists or put a file with the appropriate name into the {wmap_dir} folder."
            )
        else:
            modeldict[key] = str(pth)

    if failed:
        sys.exit(1)

    return modeldict


def validate_args(input_file: pd.DataFrame, args: argparse.ArgumentParser):
    ncols = len(scan_file.columns) - 1
    num_demvars = len(args.model_dict.keys()) - 2
    if ncols != num_demvars:
        print(
            f"There is a mismatch between the number of demographics variables you provided in the input file ({ncols}) and the number needed for your chosen model ({num_demvars}). Please check the file."
        )
        print(input_file.head())

        sys.exit(1)

    first_row_first_val = input_file.iloc[0, 0]
    if not isinstance(first_row_first_val, str):
        print(
            f"First column should be a column of strings/paths. We checked the first element and it was not a string: {first_row_first_val}."
        )
        sys.exit(1)


def print_settings():
    pass


if __name__ == "__main__":
    args = parse_args()

    scan_file = filehandling.read_wmap_csv(args.input_file)
    validate_args(scan_file, args)
    print_settings()

    tmpdir = wmap_workflow(
        scan_file,
        args.output_directory,
        args.model_dict,
        n_procs=args.n_procs,
        memory_gb=args.memory_gb,
        brainmask=args.brainmask,
    )

    if args.remove_tmpfiles:
        print("Removing tmpdir now.")
        shutil.rmtree(tmpdir)
