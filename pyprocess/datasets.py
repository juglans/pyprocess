import bids
import sys
import collections
import re
import dataclasses


def get_run_params(args):
    pass


def get_bidslayout(bids_directory):
    """[summary]
    
    Arguments:
        bids_directory {[str]} -- [description]
    
    Returns:
        [type] -- [description]
    """
    bids_layout = bids.BIDSLayout(
        bids_directory, validate=False
    )  # , index_metadata=True)
    return bids_layout

    try:
        return bids.BIDSLayout(bids_directory, validate=False)
    except ValueError:
        print(
            "BIDS layout format conversion failed. Maybe you have a problem in your dataset?"
        )
        return None


def extract_subject_dictionary(bids_layout, subject, session=None, workflow="fmriprep"):
    """[summary]
    
    Arguments:
        bids_layout {[type]} -- [description]
    
    Keyword Arguments:
        subject {[type]} -- [description] (default: {None})
        session {[type]} -- [description] (default: {None})
        workflow {str} -- [description] (default: {'fmriprep'})
    
    Returns:
        [type] -- [description]
    """

    if session:
        session = str(session).replace("ses-", "")
        sessions = [session]
        print(f"Extracting: sub-{subject}, ses-{session}.")
    else:
        sessions = bids_layout.get_sessions(subject=subject)
        num_sessions = len(sessions)
        print(f"Extracting: sub-{subject}, all sessions [{num_sessions}].")

    session_object_dict = {}
    for session in sessions:

        fmri_tr = None
        struc = bids_layout.get(
            subject=subject,
            session=session,
            return_type="file",
            suffix="T1w",
            extension=".nii.gz",
        )

        confounds = bids_layout.get(
            subject=subject,
            session=session,
            return_type="file",
            suffix="regressors",
            extension=".tsv",
        )
        aroma_noise_ics = bids_layout.get(
            subject=subject, session=session, return_type="file", suffix="AROMAnoiseICs"
        )
        aroma_components = bids_layout.get(
            subject=subject, session=session, return_type="file", suffix="mixing"
        )
        brainmask = bids_layout.get(
            subject=subject,
            session=session,
            return_type="file",
            suffix="mask",
            extension=".nii.gz",
            datatype="func",
        )
        bold_nii_images = bids_layout.get(
            subject=subject,
            session=session,
            suffix="bold",
            extension=[".nii.gz", ".nii"],
        )
        aroma_denoised_image = [
            image.path for image in bold_nii_images if "AROMA" in image.filename
        ]
        nonaroma_denoised_image = [
            image.path for image in bold_nii_images if "AROMA" not in image.filename
        ]

        fmri_tr = None
        for scan in bold_nii_images:
            if fmri_tr is not None:
                continue
            if scan.filename.endswith(".nii") or scan.filename.endswith(".nii.gz"):
                fmri_tr = scan.tags["RepetitionTime"].value
                if fmri_tr > 10:
                    fmri_tr = fmri_tr/1000.

        descriptors = [
            "confounds",
            "aroma_noise_ics",
            "aroma_components",
            "brainmask",
            "aroma_denoised_image",
            "nonaroma_denoised_image",
        ]
        raw_data = list(
            zip(
                descriptors,
                [
                    confounds,
                    aroma_noise_ics,
                    aroma_components,
                    brainmask,
                    aroma_denoised_image,
                    nonaroma_denoised_image,
                ],
            )
        )
        parsed_data = [subject, session, fmri_tr]
        for desc, data in raw_data:
            assert len(data) < 2
            # subject_data[session][desc] = next((iter(data)), [])
            parsed_data.append(next(iter(data), None))

        session_object = Session(*parsed_data)
        session_object_dict[session] = session_object
    subject_data = Subject(session_object_dict)

    return subject_data


def get_datadict_from_path(path, subject=None, session=None):
    """[summary]
    
    Arguments:
        path {[type]} -- [description]
    
    Keyword Arguments:
        subject {[type]} -- [description] (default: {None})
        session {[type]} -- [description] (default: {None})
    
    Returns:
        [type] -- [description]
    """

    bids_layout = get_bidslayout(path)
    if subject:
        datadict = {
            subject: extract_subject_dictionary(
                bids_layout, subject=subject, session=session
            )
        }
    else:
        datadict = {
            subject: extract_subject_dictionary(bids_layout, subject=subject)
            for subject in bids_layout.get_subjects()
        }

    return datadict


@dataclasses.dataclass
class Subject:
    sessions: dict


@dataclasses.dataclass
class Session:
    subject_id: str
    session_id: str
    repetition_time: float
    confounds: str
    aroma_noise_ics: str
    aroma_components: str
    brainmask: str
    aroma_image: str
    preproc_image: str
