import time 
import argparse
import functools
import os
import re
from nipype import pipeline
from nipype.interfaces import fsl
from datetime import datetime
import utils
import numpy as np


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_directory")
    parser.add_argument("--filtersubjects")
    parser.add_argument(
        "--input_filelist",
        help="Fullpath of text files corresponding to the list of files for first level anaysis.",
    )
    parser.add_argument(
        "--output_directory", help="Fullpath of desired output data directory."
    )
    parser.add_argument(
        "--seed_lists", help="List of files to be used for first level analysis."
    )
    parser.add_argument("--smoothing", type=int, default=0)

    resource_opts = parser.add_argument_group(
        "resources",
        "Flags and options to control computing resources such as number of cores and amount of memory.",
    )
    resource_opts.add_argument(
        "--n_procs", default=4, type=int, help="Number of processes to use."
    )
    resource_opts.add_argument(
        "--memory_gb",
        default=12,
        type=int,
        help="Amount of memory in GB to use in total.",
    )

    args = parser.parse_args()
    args.output_directory = os.path.abspath(args.output_directory)

    # need to check for isNone for three input args
    return args


def read_seed_file_list(seed_file):
    seed_files = []
    with open(seed_file, "r") as f:
        for line in f:
            seed_files.append(line.strip())
    seed_files = [line for line in seed_files if line is not None]
    return seed_files


def read_scan_file_list(scan_file):
    scans = []
    with open(scan_file, "r") as f:
        for line in f:
            scans.append(line.strip())
    return scans


def static_workflow(
    input_files,
    output_directory,
    seed_file_list,
    n_procs=1,
    memory_gb=10,
    smoothing_fwhm=0.0,
):
    timenow = datetime.now()

    if 'TMPDIR' in os.environ:
        tmpdir = os.environ['TMPDIR']
    else:
        tmpdir = '/seeley/imaging/data/mridata/alee/wynton-tmp'

    print(tmpdir)
    workflow = pipeline.Workflow(
        name=f"static_workflow-{timenow.year}-{timenow.month}-{timenow.day}-{timenow.hour}-{timenow.second}",
        base_dir=os.path.join(tmpdir, 'staticworkflow'),
    )
    workflow.config["execution"] = {"crashfile_format": "txt"}

    workflow_nodes = []
    for seed_index, seed_file_path in enumerate(seed_file_list):
        seed_name = (
            os.path.basename(seed_file_path).replace(".nii.gz", "").replace(".nii", "")
        )

        seed_output_directory = os.path.join(output_directory, seed_name)
        os.makedirs(seed_output_directory, exist_ok=True)

        for scan_index, scan_path in enumerate(input_files):
            scan_name = (
                os.path.basename(scan_path).replace(".nii.gz", "").replace(".nii", "")
            )

            scan_search = re.search("(sub-\w+)_.*", scan_name)
            session_search = re.search(".*_(ses-\d+)_.*", scan_name)

            if scan_search is None:
                print(
                    f"Scan {scan_search}; could not find PIDN. Please add a subject string with sub-**** to the filename so that we can index it and save it properly."
                )
                sys.exit(1)

            if session_search is None:
                print(
                    f"Scan {scan_name}; could not find session. Please add a subject string with ses-**** to the filename so that we can index it and save it properly."
                )
                sys.exit(1)

            subject_name = scan_search.group(1)
            session_name = session_search.group(1)

            static_seed_nodes = create_static_analysis(
                scan_path,
                seed_file_path,
                seed_name,
                subject_name,
                seed_output_directory,
                smoothing_fwhm=6.0,
                session_name=session_name,
            )
            workflow_nodes.append(static_seed_nodes)
    workflow.add_nodes(workflow_nodes)

    workflow.run(
        plugin="MultiProc",
        plugin_args={
            "n_procs": n_procs,
            "memory_gb": memory_gb,
            "crashfile_format": "txt",
        },
    )


def create_static_analysis(
    scan_path,
    seed_file_path,
    seed_name,
    scan_name,
    output_directory,
    smoothing_fwhm=0.0,
    session_name="",
):

    tnow_second = int(time.time())
    rand = np.random.choice(100)
    wkflow_desc_prefix = f"{scan_name}_{session_name}_{seed_name}_firstlevel-{tnow_second}-{rand}"

    if 'TMPDIR' in os.environ:
        tmpdir = os.environ['TMPDIR']
    else:
        tmpdir = '/seeley/imaging/data/mridata/alee/wynton-tmp'
    print(tmpdir)
    subworkflow = pipeline.Workflow(
        name=wkflow_desc_prefix,
        base_dir=os.path.join(tmpdir, wkflow_desc_prefix),
    )   

    subworkflow.config["execution"] = {"crashfile_format": "txt"}

    seed_identifier = seed_name.replace("_", "-")
    out_seed_meants_result = re.sub(
        "desc-.*(?=.nii.gz)",
        f"desc-{seed_identifier}_seedts",
        os.path.basename(scan_path),
    ).replace(".nii.gz", ".txt")
    out_seed_correlation_result = re.sub(
        "desc-.*(?=.nii.gz)",
        f"desc-{seed_identifier}_betas",
        os.path.basename(scan_path),
    )

    seed_ts_extractor = pipeline.Node(
        interface=fsl.ImageMeants(), name=f"{wkflow_desc_prefix}_seed-extractor"
    )
    seed_ts_extractor.inputs.in_file = scan_path
    seed_ts_extractor.inputs.mask = seed_file_path
    seed_ts_extractor.inputs.out_file = out_seed_meants_result

    betamap_generator = pipeline.Node(
        interface=fsl.GLM(), name=f"{wkflow_desc_prefix}_seed-correlation-analysis"
    )
    betamap_generator.inputs.in_file = scan_path
    betamap_generator.inputs.out_z_name = 'zstat.nii.gz'

    slicer = pipeline.Node(
        interface=fsl.utils.ExtractROI(), name=f"{wkflow_desc_prefix}_betamap-extractor"
    )
    slicer.inputs.t_min = 0
    slicer.inputs.t_size = 1
    slicer.inputs.roi_file = os.path.join(output_directory, out_seed_correlation_result)

    zstat_slicer = pipeline.Node(
        interface=fsl.utils.ExtractROI(), name=f"{wkflow_desc_prefix}_zmap-extractor"
    )
   
    zstat_slicer.inputs.t_min = 0
    zstat_slicer.inputs.t_size = 1
    zstat_slicer.inputs.roi_file = os.path.join(output_directory, out_seed_correlation_result.replace("betas", "zstat"))

    smoothing_node = pipeline.Node(
        interface=fsl.utils.Smooth(), name=f"{wkflow_desc_prefix}_firstlevel-smoothing"
    )

    smoothing_node.inputs.fwhm = smoothing_fwhm
    smoothing_node.inputs.smoothed_file = os.path.join(
        output_directory, out_seed_correlation_result.replace("betas", "sbetas")
    )

    subworkflow.connect(
        [
            (
                seed_ts_extractor,
                betamap_generator,
                [(("out_file", utils.add_intercept), "design")],
            ),
            (betamap_generator, slicer, [("out_file", "in_file")]),
            (betamap_generator, zstat_slicer, [("out_z", "in_file")]),
        ]
    )

    return subworkflow


if __name__ == "__main__":
    args = parse_args()
    seed_file_list = read_seed_file_list(args.seed_lists)
    scan_file_list = read_scan_file_list(args.input_filelist)
    static_workflow(
        scan_file_list,
        args.output_directory,
        seed_file_list,
        n_procs=args.n_procs,
        memory_gb=args.memory_gb,
        smoothing_fwhm=args.smoothing,
    )
