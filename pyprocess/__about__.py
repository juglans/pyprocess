from .__version import get_versions
__version__ = get_versions()['versions']

__packagename__ = 'pyprocess'
__url__ = 'https://gitlab.com/juglans/pyprocess

