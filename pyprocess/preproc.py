import os
import sys
import utils
import nibabel as nib
from _version import get_versions
from datetime import datetime
from nipype import pipeline
from nipype.algorithms import confounds
from nipype.interfaces import io, base, utility, fsl, afni


def single_session_wf(session_obj, basedir, noaroma=True, unifiedmodel=False, **kwargs):
    if noaroma is False:
        target_file = session_obj.aroma_image
        print(
            f"Using AROMA workflow {session_obj.subject_id}: {session_obj.session_id}. \
            Target file is {target_file}.\n"
        )
    else:
        target_file = session_obj.preproc_image
        print(
            f"Using non-AROMA workflow for {session_obj.subject_id}: {session_obj.session_id}. \
            Target file is {target_file}.\n"
        )

    if unifiedmodel:
        wkflow = create_unified_single_session_wf(
            target_file,
            session_obj.confounds,
            session_obj.repetition_time,
            basedir,
            session_obj.subject_id,
            session_obj.session_id,
            raw_data=session_obj.preproc_image,
            seed_file_list=kwargs["seedpaths"],
            **kwargs,
        )
    else:
        wkflow = create_nonunified_single_session_wf(
            target_file,
            session_obj.confounds,
            session_obj.repetition_time,
            basedir,
            session_obj.subject_id,
            session_obj.session_id,
            raw_data=session_obj.preproc_image,
            **kwargs,
        )
    return wkflow


def create_unified_single_session_wf(
    target_file,
    confound_file,
    tr,
    basedir,
    subjectid,
    sessionid,
    seed_file_list,
    tmpdir=None,
    dropvols=5,
    smoothing=0,
    detrend=False,
    acompcor=False,
    tcompcor=False,
    tissue_signals=False,
    intercept=False,
    globalsignal=False,
    susan=False,
    aroma_ic_file=None,
    aroma_noise_file=None,
    lowpass=None,
    highpass=None,
    zscore=False,
    friston24=False,
    raw_data=None,
    brainmask=None,
    **kwargs,
):
    # TODO: add fsl glm brainmasking

    version = get_versions()["version"]
    wkflow_desc_prefix = f"sub-{subjectid}_ses-{sessionid}"
    wkflow_output_path = os.path.join(basedir, f"sub-{subjectid}", f"ses-{sessionid}")
    output_suffix = ""
    desc_string = ""

    if not kwargs["fmriprep_noaroma"]:
        desc_string += "A"

    preproc_steps = {
        "PyProcess Version": version,
        "Skullstripped": "true",
        "RepetitionTime": int(tr),
        "Steps": {},
    }
    preproc_steps = utils.write_motion_summary_to_json(
        confound_file, preproc_steps, tr, dropvols=dropvols
    )

    print(
        f"Running {subjectid}, {sessionid} workflow setup-seed correlation with nuisance covariates."
    )
    print(f"\tDenoising routine:")
    print(f"\t\tDetrending: {detrend}")
    print(f"\t\tIntercept term: {intercept}")
    print(f"\t\tFriston24: {friston24}")
    print(f"\t\taCompCor: {acompcor}")
    print(f"\t\ttCompCor: {tcompcor}")
    print(f"\t\tCSF/WM: {tissue_signals}")
    print(f"\t\tGlobal Signal: {globalsignal}")
    if "AROMA" in target_file:
        print(f"\t\tSmoothing: 6mm SUSAN")
    elif smoothing != 0:
        print(f"\t\tSmoothing: {smoothing} mm isotropic")
    else:
        print("\t\tSmoothing: 0 mm")
    if lowpass == 0 and highpass == 0:
        print(f"\t\tBandpassing: False")
    else:
        print(f"\t\t\tLowpass: {lowpass} Hz")
        print(f"\t\t\tHighpass: {highpass} Hz")

    if tmpdir is None:
        if "TMPDIR" in os.environ:
            aroma_bool = not kwargs["fmriprep_noaroma"]
            tmpdir = os.path.join(
                os.environ["TMPDIR"],
                f"{wkflow_desc_prefix}_nipypreprocessing-aroma_{aroma_bool}-smoothing_{smoothing}-friston_{friston24}-detrend_{detrend}-csfwm_{tissue_signals}-lp{lowpass}-hp{highpass}-gs{globalsignal}",
            )
        elif os.path.exists("/tmp"):
            tmpdir = os.path.join("/tmp", f"{wkflow_desc_prefix}-nipypreprocessing")
            os.makedirs(tmpdir, exist_ok=True)

    if os.path.exists(tmpdir) is False:
        tmpdir = os.path.join(
            basedir, "intermediates", f"{wkflow_desc_prefix}-nipypreprocessing"
        )

    wkflow = pipeline.Workflow(
        name=f"{wkflow_desc_prefix}_preproc_workflow", base_dir=tmpdir
    )
    x, y, z, numvols = nib.load(target_file).shape

    # TODO: this piece can be written in another fn
    if (x, y, z) == (91, 109, 91):  # TODO and "space" not in fname
        space = "space-MNI152NLin6Asym"
        preproc_steps["Space"] = "MNI152NLin6Asym"
    else:
        space = "space-native_"
        preproc_steps["Space"] = "Native"
    output_suffix += f"{space}_"

    if brainmask is None:
        brainmask = "/usr/share/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz"

    print("")
    print("Setting up truncation.")

    tsize = int(numvols - dropvols)
    output_datasink = pipeline.Node(
        io.DataSink(), name=f"{wkflow_desc_prefix}_datasink"
    )
    output_datasink.inputs.base_directory = wkflow_output_path
    preproc_steps["Steps"]["Truncate"] = {"RetainedRange": f"{dropvols}-{numvols}"}

    slicer = pipeline.Node(
        interface=fsl.utils.ExtractROI(),
        name=f"{wkflow_desc_prefix}_temporal-roi-extractor",
    )
    slicer.inputs.t_min = dropvols
    slicer.inputs.t_size = tsize
    slicer.inputs.in_file = target_file
    desc_string += "T"

    print("Preprocessing confounds and selecting out:")
    confound_preprocessor = pipeline.Node(
        interface=utility.Function(
            function=utils.prepare_confounds_file,
            input_names=[
                "confound_filepath",
                "dropvols",
                "intercept",
                "detrend",
                "acompcor",
                "tcompcor",
                "tissue_signals",
                "friston24",
                "global_signal",
                "mode",
            ],
            output_names=["confounds_txt", "confounds_csv", "motion_param_csv"],
        ),
        name=f"{wkflow_desc_prefix}_confound-preprocessor",
    )

    confound_preprocessor.inputs.confound_filepath = confound_file
    confound_preprocessor.inputs.friston24 = friston24
    confound_preprocessor.inputs.dropvols = dropvols
    confound_preprocessor.inputs.intercept = intercept
    confound_preprocessor.inputs.detrend = detrend
    confound_preprocessor.inputs.tissue_signals = tissue_signals
    confound_preprocessor.inputs.acompcor = acompcor
    confound_preprocessor.inputs.tcompcor = tcompcor
    confound_preprocessor.inputs.global_signal = globalsignal

    if detrend:
        print("Detrending was flagged.")
        preproc_steps["Steps"]["Detrend"] = {"Order": "1"}

    if intercept:
        print("Intercept flag here.")

    target_tsnr_mapper = pipeline.Node(
        interface=confounds.TSNR(), name=f"{wkflow_desc_prefix}_np-target-tsnr",
    )
    target_tsnr_mapper.inputs.tsnr_file = f"{wkflow_desc_prefix}_task-rest00_acq-fullbrain-run-01_{space}_desc-dirtytsnr.nii.gz"

    tsnr_brainmasker = pipeline.Node(
        interface=fsl.maths.ApplyMask(), name=f"{wkflow_desc_prefix}_fsl-target-masker",
    )

    tsnr_brainmasker.inputs.mask_file = brainmask
    tsnr_brainmasker.inputs.in_file = raw_data if raw_data else target_file

    wkflow.connect([(tsnr_brainmasker, target_tsnr_mapper, [("out_file", "in_file")])])

    if susan:
        desc_string += "S"

        susan_masker = pipeline.Node(
            interface=fsl.ImageMaths(
                suffix="_mask", op_string="-mas", in_file2=brainmask
            ),
            name=f"{wkflow_desc_prefix}_susan_masker",
        )
        susan_median_calc = pipeline.Node(
            interface=fsl.ImageStats(op_string="-k %s -p 50", mask_file=brainmask),
            name=f"{wkflow_desc_prefix}_susan_median",
        )
        susan_meanimage_calc = pipeline.Node(
            fsl.MeanImage(), name=f"{wkflow_desc_prefix}_mean_image_calc"
        )
        susan_usans_generator = pipeline.Node(
            interface=utility.Function(
                function=utils._get_usans,
                input_names=["image", "threshold_value"],
                output_names=["usans"],
            ),
            name=f"{wkflow_desc_prefix}_get_usans",
        )
        susan_smoother = pipeline.Node(
            interface=fsl.SUSAN(fwhm=float(smoothing)),
            name=f"{wkflow_desc_prefix}_susan_smoothing",
        )

        wkflow.connect(
            [
                (slicer, susan_masker, [("roi_file", "in_file")]),
                (slicer, susan_median_calc, [("roi_file", "in_file")]),
                (susan_masker, susan_meanimage_calc, [("out_file", "in_file")]),
                (
                    susan_median_calc,
                    susan_usans_generator,
                    [("out_stat", "threshold_value")],
                ),
                (susan_meanimage_calc, susan_usans_generator, [("out_file", "image")]),
                (susan_usans_generator, susan_smoother, [("usans", "usans")]),
                (
                    susan_median_calc,
                    susan_smoother,
                    [(("out_stat", utils._get_btthresh), "brightness_threshold")],
                ),
                (slicer, susan_smoother, [("roi_file", "in_file")]),
            ]
        )

        if lowpass != 0 or highpass != 0:
            print("Setting up bandpassing node.\n")
            confound_bandpasser = pipeline.Node(
                interface=utils.OneDBandpass(),
                name=f"{wkflow_desc_prefix}_confound-bandpasser",
            )
            confound_bandpasser.inputs.dt = tr
            confound_bandpasser.inputs.no_detrend = True

            nifti_bandpasser = pipeline.Node(
                interface=afni.Bandpass(), name=f"{wkflow_desc_prefix}_nifti-bandpasser"
            )
            nifti_bandpasser.inputs.no_detrend = True
            nifti_bandpasser.inputs.outputtype = "NIFTI_GZ"
            nifti_bandpasser.inputs.tr = tr

            bp_mean_image_calc = pipeline.Node(
                interface=fsl.maths.MeanImage(),
                name=f"{wkflow_desc_prefix}_bandpasser-mean-image-calc",
            )
            bp_mean_image_calc.inputs.dimension = "T"

            remeaner = pipeline.Node(
                utility.Function(
                    function=utils.remean_image,
                    input_names=["demeaned_image", "image_with_mean"],
                    output_names=["out_file"],
                ),
                name=f"{wkflow_desc_prefix}_remeaning-node",
            )
            if highpass:
                confound_bandpasser.inputs.highpass = highpass
                nifti_bandpasser.inputs.highpass = highpass
                desc_string += "HP"

            if lowpass:
                confound_bandpasser.inputs.lowpass = lowpass
                nifti_bandpasser.inputs.lowpass = lowpass
                desc_string += "LP"

            preproc_steps["Steps"]["TemporalFiltering"] = {
                "FilterType": "fourier",
                "PassBand": f"{lowpass}:{highpass}",
            }

            wkflow.connect(
                [
                    (
                        confound_preprocessor,
                        confound_bandpasser,
                        [("confounds_txt", "in_file")],
                    ),
                    (susan_smoother, nifti_bandpasser, [("smoothed_file", "in_file")]),
                    (
                        susan_smoother,
                        bp_mean_image_calc,
                        [("smoothed_file", "in_file")],
                    ),
                    (nifti_bandpasser, remeaner, [("out_file", "demeaned_image")]),
                    (bp_mean_image_calc, remeaner, [("out_file", "image_with_mean")]),
                ]
            )

            for seed_file_path in seed_file_list:
                if friston24:
                    desc_string += "MC"
                if globalsignal:
                    desc_string += "G"

                seed_basename = (
                    os.path.basename(seed_file_path)
                    .replace(".nii.gz", "")
                    .replace(".nii", "")
                    .replace("-", "")
                    .replace("_", "")
                )
                seed_wkflow_desc_prefix = f"{wkflow_desc_prefix}-{seed_basename}"

                clean_confound_appender = pipeline.Node(
                    interface=utility.Function(
                        function=utils.demean_and_add_regressors,
                        input_names=[
                            "txt_path",
                            "csv_path",
                            "add_linear_term",
                            "add_intercept_term",
                            "seed_tseries_file",
                            "seed_name",
                        ],
                        output_names=["confounds_txt", "confounds_csv"],
                    ),
                    name=f"{seed_wkflow_desc_prefix}_clean-confound-appender",
                )
                clean_confound_appender.inputs.add_intercept_term = intercept
                clean_confound_appender.inputs.add_linear_term = detrend

                wkflow.connect(
                    [
                        (
                            confound_preprocessor,
                            clean_confound_appender,
                            [("confounds_csv", "csv_path")],
                        )
                    ]
                )

                if lowpass != 0 or highpass != 0:
                    wkflow.connect(
                        [
                            (
                                confound_bandpasser,
                                clean_confound_appender,
                                [("out_file", "txt_path")],
                            )
                        ]
                    )
                else:
                    # TODO: pass confounds_txt from confound_preprocess to clean_confound_appender
                    pass

                seed_ts_extractor = pipeline.Node(
                    interface=fsl.ImageMeants(),
                    name=f"{seed_wkflow_desc_prefix}_timeseries_extractor",
                )

                seed_ts_extractor.inputs.mask = seed_file_path
                seed_ts_extractor.inputs.out_file = f"{seed_basename}.txt"

                seed_betamap_extractor = pipeline.Node(
                    interface=fsl.utils.ExtractROI(),
                    name=f"{seed_wkflow_desc_prefix}_betamap_extractor",
                )
                seed_betamap_extractor.inputs.t_min = 0
                seed_betamap_extractor.inputs.t_size = 1
                seed_betamap_extractor.inputs.roi_file = (
                    os.path.basename(target_file).split("_desc")[0]
                    + f"_seed-{seed_basename}_betas.nii.gz"
                )

                glm_unified_model = pipeline.Node(
                    interface=fsl.GLM(),
                    name=f"{seed_wkflow_desc_prefix}_fsl-glm-regression",
                )
                if brainmask:
                    glm_unified_model.inputs.mask = brainmask
                glm_unified_model.inputs.out_file = (
                    os.path.basename(target_file).split("_desc")[0]
                    + f"_seed-{seed_basename}_parameters.nii.gz"
                )

                wkflow.connect(
                    [
                        (remeaner, seed_ts_extractor, [("out_file", "in_file")]),
                        (
                            seed_ts_extractor,
                            clean_confound_appender,
                            [("out_file", "seed_tseries_file")],
                        ),
                        (
                            clean_confound_appender,
                            glm_unified_model,
                            [("confounds_txt", "design")],
                        ),
                        (
                            clean_confound_appender,
                            output_datasink,
                            [("confounds_csv", f"func.@{seed_basename}confounds")],
                        ),
                        (remeaner, glm_unified_model, [("out_file", "in_file")]),
                        (
                            glm_unified_model,
                            seed_betamap_extractor,
                            [("out_file", "in_file")],
                        ),
                        (
                            glm_unified_model,
                            output_datasink,
                            [("out_file", f"func.@{seed_basename}-allbetas")],
                        ),
                        (
                            seed_betamap_extractor,
                            output_datasink,
                            [("roi_file", f"func.@{seed_basename}-seedbetas")],
                        ),
                    ]
                )

        else:
            raise (
                NotImplementedError,
                "SUSAN without bandpassing not implemented yet.",
            )
            # susan but no bp
    elif not susan:
        if "AROMA" in target_file:
            desc_string += "S"

        if lowpass != 0 or highpass != 0:
            print("Setting up bandpassing node.\n")
            confound_bandpasser = pipeline.Node(
                interface=utils.OneDBandpass(),
                name=f"{wkflow_desc_prefix}_confound-bandpasser",
            )
            confound_bandpasser.inputs.dt = tr
            confound_bandpasser.inputs.no_detrend = True

            nifti_bandpasser = pipeline.Node(
                interface=afni.Bandpass(), name=f"{wkflow_desc_prefix}_nifti-bandpasser"

            )
            nifti_bandpasser.inputs.no_detrend = True
            nifti_bandpasser.inputs.outputtype = "NIFTI_GZ"
            nifti_bandpasser.inputs.tr = tr

            bp_mean_image_calc = pipeline.Node(
                interface=fsl.maths.MeanImage(),
                name=f"{wkflow_desc_prefix}_bandpasser-mean-image-calc",
            )
            bp_mean_image_calc.inputs.dimension = "T"

            remeaner = pipeline.Node(
                utility.Function(
                    function=utils.remean_image,
                    input_names=["demeaned_image", "image_with_mean"],
                    output_names=["out_file"],
                ),
                name=f"{wkflow_desc_prefix}_remeaning-node",
            )

            if highpass:
                confound_bandpasser.inputs.highpass = highpass
                nifti_bandpasser.inputs.highpass = highpass
                desc_string += "HP"

            if lowpass:
                confound_bandpasser.inputs.lowpass = lowpass
                nifti_bandpasser.inputs.lowpass = lowpass
                desc_string += "LP"

            preproc_steps["Steps"]["TemporalFiltering"] = {
                "FilterType": "fourier",
                "PassBand": f"{lowpass}:{highpass}",
            }

            wkflow.connect(
                [
                    (
                        confound_preprocessor,
                        confound_bandpasser,
                        [("confounds_txt", "in_file")],
                    )
                ]
            )

            if smoothing != 0:

                desc_string += "S"

                fsl_smoothing_node = pipeline.Node(
                    fsl.maths.IsotropicSmooth(),
                    name=f"{wkflow_desc_prefix}_fsl-isotropic_smooth",
                )
                fsl_smoothing_node.inputs.fwhm = float(smoothing)

                wkflow.connect(
                    [
                        (slicer, fsl_smoothing_node, [("roi_file", "in_file")]),
                        (
                            fsl_smoothing_node,
                            nifti_bandpasser,
                            [("out_file", "in_file")],
                        ),
                        (nifti_bandpasser, remeaner, [("out_file", "demeaned_image")]),
                        (
                            fsl_smoothing_node,
                            bp_mean_image_calc,
                            [("out_file", "in_file")],
                        ),
                        (
                            bp_mean_image_calc,
                            remeaner,
                            [("out_file", "image_with_mean")],
                        ),
                    ]
                )

            else:  # bandpassing but no smoothing
                wkflow.connect(
                    [
                        (slicer, bp_mean_image_calc, [("roi_file", "in_file")]),
                        (slicer, nifti_bandpasser, [("roi_file", "in_file")]),
                        (nifti_bandpasser, remeaner, [("out_file", "demeaned_image")]),
                        (
                            bp_mean_image_calc,
                            remeaner,
                            [("out_file", "image_with_mean")],
                        ),
                    ]
                )

            for seed_file_path in seed_file_list:
                if friston24:
                    wkflow_desc_prefix += "MC"
                if globalsignal:
                    wkflow_desc_prefix += "G"

                seed_basename = (
                    os.path.basename(seed_file_path)
                    .replace(".nii.gz", "")
                    .replace(".nii", "")
                    .replace("-", "")
                    .replace("_", "")
                )
                seed_wkflow_desc_prefix = f"{wkflow_desc_prefix}-{seed_basename}"

                clean_confound_appender = pipeline.Node(
                    interface=utility.Function(
                        function=utils.demean_and_add_regressors,
                        input_names=[
                            "txt_path",
                            "csv_path",
                            "add_linear_term",
                            "add_intercept_term",
                            "seed_tseries_file",
                            "seed_name",
                        ],
                        output_names=["confounds_txt", "confounds_csv"],
                    ),
                    name=f"{seed_wkflow_desc_prefix}_clean-confound-appender",
                )
                clean_confound_appender.inputs.add_intercept_term = intercept
                clean_confound_appender.inputs.add_linear_term = detrend

                wkflow.connect(
                    [
                        (
                            confound_preprocessor,
                            clean_confound_appender,
                            [("confounds_csv", "csv_path")],
                        )
                    ]
                )
                wkflow.connect(
                    [
                        (
                            confound_bandpasser,
                            clean_confound_appender,
                            [("out_file", "txt_path")],
                        )
                    ]
                )

                seed_ts_extractor = pipeline.Node(
                    interface=fsl.ImageMeants(),
                    name=f"{seed_wkflow_desc_prefix}_timeseries_extractor",
                )
                seed_ts_extractor.inputs.mask = seed_file_path
                seed_ts_extractor.inputs.out_file = f"{seed_basename}.txt"

                seed_betamap_extractor = pipeline.Node(
                    interface=fsl.utils.ExtractROI(),
                    name=f"{seed_wkflow_desc_prefix}_betamap_extractor",
                )
                seed_betamap_extractor.inputs.t_min = 0
                seed_betamap_extractor.inputs.t_size = 1
                seed_betamap_extractor.inputs.roi_file = (
                    os.path.basename(target_file).split("_desc")[0]
                    + f"_seed-{seed_basename}_betas.nii.gz"
                )

                glm_unified_model = pipeline.Node(
                    interface=fsl.GLM(),
                    name=f"{seed_wkflow_desc_prefix}_fsl-glm-regression",
                )
                if brainmask:
                    glm_unified_model.inputs.mask = brainmask
                glm_unified_model.inputs.out_file = (
                    os.path.basename(target_file).split("_desc")[0]
                    + f"_seed-{seed_basename}_parameters.nii.gz"
                )

                wkflow.connect(
                    [
                        (remeaner, seed_ts_extractor, [("out_file", "in_file")]),
                        (
                            seed_ts_extractor,
                            clean_confound_appender,
                            [("out_file", "seed_tseries_file")],
                        ),
                        (
                            clean_confound_appender,
                            glm_unified_model,
                            [("confounds_txt", "design")],
                        ),
                        (
                            clean_confound_appender,
                            output_datasink,
                            [("confounds_csv", f"func.@{seed_basename}confounds")],
                        ),
                        (remeaner, glm_unified_model, [("out_file", "in_file")]),
                        (
                            glm_unified_model,
                            seed_betamap_extractor,
                            [("out_file", "in_file")],
                        ),
                        (
                            glm_unified_model,
                            output_datasink,
                            [("out_file", f"func.@{seed_basename}-allbetas")],
                        ),
                        (
                            seed_betamap_extractor,
                            output_datasink,
                            [("roi_file", f"func.@{seed_basename}-seedbetas")],
                        ),
                        (
                            clean_confound_appender,
                            output_datasink,
                            [("confounds_csv", "func.@clean_confounds")],
                        ),
                    ]
                )

            else:
                # no susan no bandpassing
                raise (
                    NotImplementedError,
                    "No smoothing, no bandpassing not implemented yet. Try passing --susan for now.",
                )

    tsnr_suffix = output_suffix + f"desc-"
    output_suffix += f"desc-{desc_string}_bold"

    json_writer = pipeline.Node(
        interface=utility.Function(
            function=utils.write_denoising_desc_json,
            input_names=["json_dict", "desc_string", "target_filename"],
            output_names=["out_json"],
        ),
        name=f"{wkflow_desc_prefix}_json_writer",
    )

    json_writer.inputs.json_dict = preproc_steps
    json_writer.inputs.desc_string = output_suffix
    json_writer.inputs.target_filename = target_file

    diagnostics_plotter = pipeline.Node(
        interface=utility.Function(
            function=utils.create_movement_plots,
            input_names=["movement_csv", "dirty_snr_map", "clean_snr_map"],
            output_names=["timeseries_plot", "dirty_snr_plot", "clean_snr_map"],
        ),
        name=f"{wkflow_desc_prefix}_diagnostic_plotter",
    )

    wkflow.connect(
        [
            (
                confound_preprocessor,
                output_datasink,
                [("motion_param_csv", "func.@motion_params")],
            ),
            # (
            #    clean_confound_appender,
            #    output_datasink,
            #    [("confounds_csv", "func.@clean_confounds")],
            # ),
            (json_writer, output_datasink, [("out_json", "func.@json")]),
            (target_tsnr_mapper, output_datasink, [("tsnr_file", "func.@dtsnr")]),
            (target_tsnr_mapper, diagnostics_plotter, [("tsnr_file", "dirty_snr_map")]),
            (
                confound_preprocessor,
                diagnostics_plotter,
                [("motion_param_csv", "movement_csv")],
            ),
            (
                diagnostics_plotter,
                output_datasink,
                [("timeseries_plot", "func.@tsplot")],
            ),
            (diagnostics_plotter, output_datasink, [("dirty_snr_plot", "func.@dplot")]),
        ]
    )

    output_datasink.inputs.regexp_substitutions = [
        ("space.*residuals.*(?<!std).nii.gz", output_suffix + ".nii.gz"),
        ("space.*residuals.*bold.*std.nii.gz", tsnr_suffix + "cleantsnr.nii.gz"),
        (
            "space.*(.*smoothAROMA*|.*preproc.*).*.nii.gz",
            tsnr_suffix + "dirtytsnr.nii.gz",
        ),
        ("_desc.*tsv", "_desc-confounds_cleaned.csv"),
    ]

    return wkflow


def create_nonunified_single_session_wf(
    target_file,
    confound_file,
    tr,
    basedir,
    subjectid,
    sessionid,
    tmpdir=None,
    dropvols=5,
    smoothing=0,
    detrend=False,
    acompcor=False,
    tcompcor=False,
    tissue_signals=False,
    intercept=False,
    globalsignal=False,
    susan=False,
    aroma_ic_file=None,
    aroma_noise_file=None,
    lowpass=0,
    highpass=0,
    zscore=False,
    friston24=False,
    raw_data=None,
    brainmask=None,
    **kwargs,
):

    # TODO: add fsl glm brainmasking
    version = get_versions()["version"]
    wkflow_desc_prefix = f"sub-{subjectid}_ses-{sessionid}"
    wkflow_output_path = os.path.join(basedir, f"sub-{subjectid}", f"ses-{sessionid}")
    output_suffix = ""
    desc_string = ""
    if not kwargs["fmriprep_noaroma"]:
        desc_string += "A"
    preproc_steps = {
        "PyProcess Version": version,
        "Skullstripped": "true",
        "RepetitionTime": int(tr),
        "Steps": {},
    }
    preproc_steps = utils.write_motion_summary_to_json(
        confound_file, preproc_steps, tr, dropvols=dropvols
    )

    print(f"Running {subjectid}, {sessionid} workflow setup-just nuisance regression.")
    print(f"\tDenoising routine:")
    print(f"\t\tDetrending: {detrend}")
    print(f"\t\tIntercept term: {intercept}")
    print(f"\t\tFriston24: {friston24}")
    print(f"\t\taCompCor: {acompcor}")
    print(f"\t\ttCompCor: {tcompcor}")
    print(f"\t\tCSF/WM: {tissue_signals}")
    print(f"\t\tGlobal Signal: {globalsignal}")
    if "AROMA" in target_file:
        print(f"\t\tSmoothing: 6mm SUSAN")
    elif smoothing != 0:
        print(f"\t\tSmoothing: {smoothing} mm isotropic")
    else:
        print("\t\tSmoothing: 0 mm")
    if lowpass == 0 and highpass == 0:
        print(f"\t\tBandpassing: False")
    else:
        print(f"\t\t\tLowpass: {lowpass} Hz")
        print(f"\t\t\tHighpass: {highpass} Hz")

    if tmpdir is None:
        if "TMPDIR" in os.environ:
            if "AROMA" in target_file:
                aroma = True
            else:
                aroma = False
            tmpdir = os.path.join(
                os.environ["TMPDIR"],
                f"{wkflow_desc_prefix}_nipypreprocessing-aroma_{aroma}-smoothing_{smoothing}-friston_{friston24}-detrend_{detrend}-csfwm_{tissue_signals}-lp{lowpass}-hp{highpass}-gs{globalsignal}",
            )
        elif os.path.exists("/tmp"):
            tmpdir = os.path.join("/tmp", f"{wkflow_desc_prefix}-nipypreprocessing")
            os.makedirs(tmpdir, exist_ok=True)

    if os.path.exists(tmpdir) is False:
        tmpdir = os.path.join(
            basedir, "intermediates", f"{wkflow_desc_prefix}-nipypreprocessing"
        )

    wkflow = pipeline.Workflow(
        name=f"{wkflow_desc_prefix}_preproc_workflow", base_dir=tmpdir
    )
    x, y, z, numvols = nib.load(target_file).shape

    # TODO: this piece can be written in another fn
    if (x, y, z) == (91, 109, 91):  # TODO and "space" not in fname
        space = "space-MNI152NLin6Asym"
        preproc_steps["Space"] = "MNI152NLin6Asym"
    else:
        space = "space-native_"
        preproc_steps["Space"] = "Native"
    output_suffix += f"{space}_"

    if brainmask is None:
        brainmask = "/usr/share/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz"

    print("")
    print("\t\tSetting up truncation.")

    tsize = int(numvols - dropvols)
    output_datasink = pipeline.Node(
        io.DataSink(), name=f"{wkflow_desc_prefix}_datasink"
    )
    output_datasink.inputs.base_directory = wkflow_output_path
    preproc_steps["Steps"]["Truncate"] = {"RetainedRange": f"{dropvols}-{numvols}"}

    slicer = pipeline.Node(
        interface=fsl.utils.ExtractROI(),
        name=f"{wkflow_desc_prefix}_temporal-roi-extractor",
    )
    slicer.inputs.t_min = dropvols
    slicer.inputs.t_size = tsize
    slicer.inputs.in_file = target_file
    desc_string += "T"

    print("\t\tSetting up confounds workflow.")
    confound_preprocessor = pipeline.Node(
        interface=utility.Function(
            function=utils.prepare_confounds_file,
            input_names=[
                "confound_filepath",
                "dropvols",
                "intercept",
                "detrend",
                "acompcor",
                "tcompcor",
                "tissue_signals",
                "friston24",
                "global_signal",
                "mode",
            ],
            output_names=["confounds_txt", "confounds_csv", "motion_param_csv"],
        ),
        name=f"{wkflow_desc_prefix}_confound-preprocessor",
    )

    confound_preprocessor.inputs.confound_filepath = confound_file
    confound_preprocessor.inputs.friston24 = friston24
    confound_preprocessor.inputs.dropvols = dropvols
    confound_preprocessor.inputs.intercept = intercept
    confound_preprocessor.inputs.detrend = detrend
    confound_preprocessor.inputs.tissue_signals = tissue_signals
    confound_preprocessor.inputs.acompcor = acompcor
    confound_preprocessor.inputs.tcompcor = tcompcor
    confound_preprocessor.inputs.global_signal = globalsignal

    print("\t\tDetrending was flagged.")
    if detrend:
        preproc_steps["Steps"]["Detrend"] = {"Order": "1"}

    print("\t\tIntercept flag here.")
    clean_confound_appender = pipeline.Node(
        interface=utility.Function(
            function=utils.demean_and_add_regressors,
            input_names=[
                "txt_path",
                "csv_path",
                "add_linear_term",
                "add_intercept_term",
                "seed_tseries_file",
                "seed_name",
            ],
            output_names=["confounds_txt", "confounds_csv"],
        ),
        name=f"{wkflow_desc_prefix}_clean-confound-appender",
    )
    clean_confound_appender.inputs.add_linear_term = detrend
    clean_confound_appender.inputs.add_intercept_term = intercept
    clean_confound_appender.inputs.seed_name = None

    glm_denoising = pipeline.Node(
        interface=fsl.GLM(), name=f"{wkflow_desc_prefix}_fsl-glm-regression"
    )

    glm_denoising.inputs.out_res_name = (
        os.path.basename(target_file).split("_desc")[0] + "-residuals_bold.nii.gz"
    )
    glm_denoising.inputs.out_file = (
        os.path.basename(target_file).split("_desc")[0] + "-nuisanceparams_bold.nii.gz"
    )
    if brainmask:
        glm_denoising.inputs.mask = brainmask

    target_tsnr_mapper = pipeline.Node(
        interface=confounds.TSNR(), name=f"{wkflow_desc_prefix}_np-target-tsnr"
    )
    target_tsnr_mapper.inputs.tsnr_file = f"{wkflow_desc_prefix}_task-rest00_acq-fullbrain_run-01_{space}_desc-dirtytsnr.nii.gz"

    tsnr_brainmasker = pipeline.Node(
        interface=fsl.maths.ApplyMask(), name=f"{wkflow_desc_prefix}_fsl-target-masker"
    )
    tsnr_brainmasker.inputs.mask_file = brainmask
    tsnr_brainmasker.inputs.in_file = raw_data if raw_data is not None else target_file

    wkflow.connect([(tsnr_brainmasker, target_tsnr_mapper, [("out_file", "in_file")])])

    cleaned_tsnr_mapper = pipeline.Node(
        interface=confounds.TSNR(), name=f"{wkflow_desc_prefix}_np-clean-tsnr"
    )
    cleaned_tsnr_mapper.inputs.tsnr_file = f"{wkflow_desc_prefix}_task-rest00_acq-fullbrain_run-01_{space}_desc-cleantsnr.nii.gz"

    if susan:
        desc_string += "S"

        susan_masker = pipeline.Node(
            interface=fsl.ImageMaths(
                suffix="_mask", op_string="-mas", in_file2=brainmask
            ),
            name=f"{wkflow_desc_prefix}_susan_masker",
        )
        susan_median_calc = pipeline.Node(
            interface=fsl.ImageStats(op_string="-k %s -p 50", mask_file=brainmask),
            name=f"{wkflow_desc_prefix}_susan_median",
        )
        susan_meanimage_calc = pipeline.Node(
            interface=fsl.ImageMaths(suffix="_mean", op_string="-Tmean"),
            name=f"{wkflow_desc_prefix}_mean_image_calc",
        )
        susan_usans_generator = pipeline.Node(
            interface=utility.Function(
                function=utils._get_usans,
                input_names=["image", "threshold_value"],
                output_names=["usans"],
            ),
            name=f"{wkflow_desc_prefix}_get_usans",
        )
        susan_smoother = pipeline.Node(
            interface=fsl.SUSAN(fwhm=float(smoothing)),
            name=f"{wkflow_desc_prefix}_susan_smoothing",
        )

        wkflow.connect(
            [
                (slicer, susan_masker, [("roi_file", "in_file")]),
                (slicer, susan_median_calc, [("roi_file", "in_file")]),
                (susan_masker, susan_meanimage_calc, [("out_file", "in_file")]),
                (
                    susan_median_calc,
                    susan_usans_generator,
                    [("out_stat", "threshold_value")],
                ),
                (susan_meanimage_calc, susan_usans_generator, [("out_file", "image")]),
                (susan_usans_generator, susan_smoother, [("usans", "usans")]),
                (
                    susan_median_calc,
                    susan_smoother,
                    [(("out_stat", utils._get_btthresh), "brightness_threshold")],
                ),
                (slicer, susan_smoother, [("roi_file", "in_file")]),
            ]
        )

        if lowpass != 0 or highpass != 0:
            print("\t\tSetting up bandpassing node.\n")
            confound_bandpasser = pipeline.Node(
                interface=utils.OneDBandpass(),
                name=f"{wkflow_desc_prefix}_confound-bandpasser",
            )
            confound_bandpasser.inputs.dt = tr
            confound_bandpasser.inputs.no_detrend = True

            nifti_bandpasser = pipeline.Node(
                interface=afni.Bandpass(), name=f"{wkflow_desc_prefix}_nifti-bandpasser"
            )
            nifti_bandpasser.inputs.no_detrend = True
            nifti_bandpasser.inputs.outputtype = "NIFTI_GZ"
            nifti_bandpasser.inputs.tr = tr

            bp_mean_image_calc = pipeline.Node(
                interface=fsl.maths.MeanImage(),
                name=f"{wkflow_desc_prefix}_bandpasser-mean-image-calc",
            )
            bp_mean_image_calc.inputs.dimension = "T"

            remeaner = pipeline.Node(
                utility.Function(
                    function=utils.remean_image,
                    input_names=["demeaned_image", "image_with_mean"],
                    output_names=["out_file"],
                ),
                name=f"{wkflow_desc_prefix}_remeaning-node",
            )

            if highpass != 0:
                confound_bandpasser.inputs.highpass = highpass
                nifti_bandpasser.inputs.highpass = highpass
                desc_string += "HP"

            if lowpass != 0:
                confound_bandpasser.inputs.lowpass = lowpass
                nifti_bandpasser.inputs.lowpass = lowpass
                desc_string += "LP"

            if friston24:
                desc_string += "MC"
            if globalsignal:
                desc_string += "G"

            preproc_steps["Steps"]["TemporalFiltering"] = {
                "FilterType": "fourier",
                "PassBand": f"{lowpass}:{highpass}",
            }

            wkflow.connect(
                [
                    (
                        confound_preprocessor,
                        confound_bandpasser,
                        [("confounds_txt", "in_file")],
                    ),
                    (
                        confound_bandpasser,
                        clean_confound_appender,
                        [("out_file", "txt_path")],
                    ),
                    (
                        confound_preprocessor,
                        clean_confound_appender,
                        [("confounds_csv", "csv_path")],
                    ),
                    (
                        clean_confound_appender,
                        glm_denoising,
                        [("confounds_txt", "design")],
                    ),
                    (
                        susan_smoother,
                        bp_mean_image_calc,
                        [("smoothed_file", "in_file")],
                    ),
                    (nifti_bandpasser, remeaner, [("out_file", "demeaned_image")]),
                    (bp_mean_image_calc, remeaner, [("out_file", "image_with_mean")]),
                    (susan_smoother, nifti_bandpasser, [("smoothed_file", "in_file")]),
                    (remeaner, glm_denoising, [("out_file", "in_file")]),
                    (glm_denoising, cleaned_tsnr_mapper, [("out_res", "in_file")]),
                    (
                        glm_denoising,
                        output_datasink,
                        [("out_res", "func.@smoothed_residuals")],
                    ),
                ]
            )
        else:  # no bandpassing, but with susan
            wkflow.connect(
                [
                    (susan_smoother, glm_denoising, [("smoothed_file", "in_file")]),
                    (
                        clean_confound_appender,
                        glm_denoising,
                        [("confounds_txt", "design")],
                    ),
                ]
            )

    elif lowpass != 0 or highpass != 0:  # no susan, but bandpassing
        if "AROMA" in target_file:
            desc_string += "S"

        print("Setting up bandpassing node.\n")
        confound_bandpasser = pipeline.Node(
            interface=utils.OneDBandpass(),
            name=f"{wkflow_desc_prefix}_confound-bandpasser",
        )
        confound_bandpasser.inputs.dt = tr
        confound_bandpasser.inputs.no_detrend = True

        nifti_bandpasser = pipeline.Node(
            interface=afni.Bandpass(), name=f"{wkflow_desc_prefix}_nifti-bandpasser"
        )
        nifti_bandpasser.inputs.tr = tr
        nifti_bandpasser.inputs.no_detrend = True
        nifti_bandpasser.inputs.outputtype = "NIFTI_GZ"
        # if smoothing != 0:
        #    nifti_bandpasser.inputs.blur = smoothing
        #    nifti_bandpasser.inputs.mask = brainmask

        if highpass:
            confound_bandpasser.inputs.highpass = highpass
            nifti_bandpasser.inputs.highpass = highpass
            desc_string += "HP"

        if lowpass:
            confound_bandpasser.inputs.lowpass = lowpass
            nifti_bandpasser.inputs.lowpass = lowpass
            desc_string += "LP"

        if friston24:
            desc_string += "MC"
        if globalsignal:
            desc_string += "G"

        preproc_steps["Steps"]["TemporalFiltering"] = {
            "FilterType": "fourier",
            "PassBand": f"{lowpass}:{highpass}",
        }
        wkflow.connect(
            [
                (
                    confound_preprocessor,
                    confound_bandpasser,
                    [("confounds_txt", "in_file")],
                ),
                (
                    confound_bandpasser,
                    clean_confound_appender,
                    [("out_file", "txt_path")],
                ),
                (
                    confound_preprocessor,
                    clean_confound_appender,
                    [("confounds_csv", "csv_path")],
                ),
                (clean_confound_appender, glm_denoising, [("confounds_txt", "design")]),
                (slicer, nifti_bandpasser, [("roi_file", "in_file")]),
                (nifti_bandpasser, glm_denoising, [("out_file", "in_file")]),
            ]
        )

    else:  # no bandpassing, no susan
        print("Setting up denoising regression.")
        wkflow.connect(
            [
                (
                    confound_preprocessor,
                    clean_confound_appender,
                    [("confounds_txt", "txt_path")],
                ),
                (
                    confound_preprocessor,
                    clean_confound_appender,
                    [("confounds_csv", "csv_path")],
                ),
                (slicer, glm_denoising, [("roi_file", "in_file")]),
                (clean_confound_appender, glm_denoising, [("confounds_txt", "design")]),
            ]
        )

    wkflow.connect(
        [
            (
                clean_confound_appender,
                output_datasink,
                [("confounds_csv", "func.@clean_confounds")],
            ),
            (
                confound_preprocessor,
                output_datasink,
                [("motion_param_csv", "func.@motion_params")],
            ),
        ]
    )

    if smoothing == 0 or smoothing is False:
        wkflow.connect(
            [
                (glm_denoising, output_datasink, [("out_res", "func.@residuals")]),
                (glm_denoising, cleaned_tsnr_mapper, [("out_res", "in_file")]),
            ]
        )

    elif susan is False and smoothing != 0:
        print("Setting up smoothing.")
        smooth = pipeline.Node(
            interface=fsl.IsotropicSmooth(),
            name=f"{wkflow_desc_prefix}_fsl-isotropic-smooth",
        )
        smooth.inputs.fwhm = smoothing

        wkflow.connect(
            [
                (glm_denoising, smooth, [("out_res", "in_file")]),
                (smooth, output_datasink, [("out_file", "func.@smoothed_residuals")]),
                (smooth, cleaned_tsnr_mapper, [("out_file", "in_file")]),
            ]
        )

        desc_string += "S"

        if zscore:
            desc_string += "Z"
            # connect
        else:
            wkflow.connect(
                [(glm_denoising, output_datasink, [("out_res", "func.@residuals")]),]
            )

    tsnr_suffix = output_suffix + f"desc-"
    output_suffix += f"desc-{desc_string}_bold"

    json_writer = pipeline.Node(
        interface=utility.Function(
            function=utils.write_denoising_desc_json,
            input_names=["json_dict", "desc_string", "target_filename"],
            output_names=["out_json"],
        ),
        name=f"{wkflow_desc_prefix}_json_writer",
    )

    json_writer.inputs.json_dict = preproc_steps
    json_writer.inputs.desc_string = output_suffix
    json_writer.inputs.target_filename = target_file

    diagnostics_plotter = pipeline.Node(
        interface=utility.Function(
            function=utils.create_movement_plots,
            input_names=["movement_csv", "dirty_snr_map", "clean_snr_map"],
            output_names=["timeseries_plot", "dirty_snr_plot", "clean_snr_plot"],
        ),
        name=f"{wkflow_desc_prefix}_diagnostic_plotter",
    )

    wkflow.connect(
        [
            (json_writer, output_datasink, [("out_json", "func.@json")]),
            (target_tsnr_mapper, output_datasink, [("tsnr_file", "func.@dtsnr")]),
            (cleaned_tsnr_mapper, output_datasink, [("tsnr_file", "func.@ctsnr")]),
            (target_tsnr_mapper, diagnostics_plotter, [("tsnr_file", "dirty_snr_map")]),
            (
                cleaned_tsnr_mapper,
                diagnostics_plotter,
                [("tsnr_file", "clean_snr_map")],
            ),
            (
                confound_preprocessor,
                diagnostics_plotter,
                [("motion_param_csv", "movement_csv")],
            ),
            (
                diagnostics_plotter,
                output_datasink,
                [("timeseries_plot", "func.@tsplot")],
            ),
            (diagnostics_plotter, output_datasink, [("dirty_snr_plot", "func.@dplot")]),
            (diagnostics_plotter, output_datasink, [("clean_snr_plot", "func.@cplot")]),
        ]
    )

    output_datasink.inputs.regexp_substitutions = [
        ("space.*residuals.*(?<!std).nii.gz", output_suffix + ".nii.gz"),
        ("space.*residuals.*bold.*std.nii.gz", tsnr_suffix + "cleantsnr.nii.gz"),
        (
            "space.*(.*smoothAROMA*|.*preproc.*).*.nii.gz",
            tsnr_suffix + "dirtytsnr.nii.gz",
        ),
        ("_desc.*tsv", "_desc-confounds_cleaned.csv"),
    ]
    print("\t\tFinished setup.\n")
    return wkflow


def create_main_workflow(basedir, name=None, kwargs=None):
    if name is None:
        name = "working_dir"
        timenow = datetime.now()
        name = f"{name}_{timenow.year}-{timenow.month}-{timenow.day}-{timenow.hour}-{timenow.second}-{timenow.microsecond}"
    workflow = pipeline.Workflow(name=name, base_dir=basedir)
    workflow.config["execution"] = {"crashfile_format": "txt"}
    if kwargs:
        workflow.config.update(kwargs)
    return workflow


def preprocess(datadict, args):
    timenow = datetime.now()
    name = (
        f"{timenow.year}-{timenow.month}-{timenow.day}-{timenow.hour}-{timenow.second}"
    )
    basedir = os.path.join(
        os.path.dirname(args.output_directory), "working"
    )  # f'working-{name}')
    if args.working:
        main_workflow = create_main_workflow(args.working)
    else:
        main_workflow = create_main_workflow(basedir)
    run_params = vars(args)  # utils.get_run_params(args)
    for subject in datadict.keys():
        # print(datadict[subject])
        for session_id, session in datadict[subject].sessions.items():
            main_workflow.add_nodes(
                [
                    single_session_wf(
                        session,
                        args.output_directory,
                        noaroma=args.fmriprep_noaroma,
                        **run_params,
                    )
                ]
            )
    main_workflow.run(
        plugin="MultiProc",
        plugin_args={
            "n_procs": args.n_procs,
            "memory_gb": args.memory_gb,
            "crashfile_format": "txt",
        },
    )
