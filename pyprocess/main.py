import argparse
import os
import sys
import utils
import datasets
import preproc
import pathlib


def parse_args():
    """
    Parse arguments related to input and output data directories, options for denoising of FMRIPREP or HCP 4D fMRI images, and optional post-denoising analysis.
    
    Returns:
        [ArgumentParser] -- ArgumentParser object.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_directory", help="Fullpath of input directory. BIDS format."
    )
    parser.add_argument(
        "output_directory", nargs="?", help="Fullpath of desired output data directory."
    )

    run_level_opts = parser.add_argument_group(
        "run_level", "Run level options to specify a single subject and/or session."
    )
    run_level_opts.add_argument(
        "--subject",
        help='Subject ID of specific subject. Can be numeric or the string "sub-#####".',
    )
    run_level_opts.add_argument(
        "--session",
        help='Session ID of specific session. Must be passed with subject. Must follow format: "ses-####".',
    )
    run_level_opts.add_argument(
        "--idfile",
        help="Path for list of 4D image files for post-denoising analysis (SCA, sliding window analysis, correlation matrices etc).",
    )

    struc_opts = parser.add_argument_group(
        'structural',
        'sMRI preprocessing options.'
    )

    struc_opts.add_argument(
        '--struc', action='store_true', help='Toggle segmentation of structural images with SPM VBM unified segmentation (no DARTEL).'
    )
    
#    struc_opts.add_argument(
#        ('--strucsmooth', default=8, help='Smoothing kernel size in mm. Default is 8mm smoothing.')
#    )

    bandpassing_opts = parser.add_argument_group(
        "bandpassing",
        "Bandpassing options for  preprocessing. To be performed simultaneously with nuisance regression.",
    )
    bandpassing_opts.add_argument(
        "--lowpass",
        "-lp",
        type=float,
        default=0.08,
        help="Low pass filter for bandpassing.",
    )
    bandpassing_opts.add_argument(
        "--highpass",
        "-hp",
        type=float,
        default=0.008,
        help="High pass filter for bandpassing.",
    )
    bandpassing_opts.add_argument(
        "--nobandpass",
        action="store_true",
        default=False,
        help="Turn off bandpassing. Default behavior is to bandpass between 0.008 Hz and 0.08 Hz.",
    )

    resource_opts = parser.add_argument_group(
        "resources",
        "Flags and options to control computing resources such as number of cores and amount of memory.",
    )
    resource_opts.add_argument(
        "--n_procs", "-n", default=3, type=int, help="Number of processes to use."
    )
    resource_opts.add_argument(
        "--memory_gb",
        "-m",
        default=8,
        type=int,
        help="Amount of memory in GB to use in total.",
    )

    nuisance_variables = parser.add_argument_group(
        "nuisance variables", "Flags and options to control nuisance regression."
    )
    nuisance_variables.add_argument(
        "--nodetrend",
        action="store_true",
        default=False,
        help="Turn off linear detrending of nuisance regressors. Default is to detrend nuisance regressors.",
    )
    nuisance_variables.add_argument(
        "--nointercept",
        action="store_true",
        default=False,
        help="Turn off fitting of an intercept term in the nuisance regression models. Default is to use an intercept.",
    )
    nuisance_variables.add_argument(
        "--acompcor",
        action="store_true",
        default=False,
        help="Use aCompCor and associated cosine bases from FMRIPREP in the nuisance regression models. Default is False.",
    )
    nuisance_variables.add_argument(
        "--tcompcor",
        action="store_true",
        default=False,
        help="Use tCompCor and associated cosine bases from FMRIPREP in the nuisance regression models. Default is False.",
    )
    nuisance_variables.add_argument(
        "--tissue_signals",
        action="store_true",
        default=False,
        help="Use mean tissue signals (via  CSF + WM eroded masks from FMRIPREP) in nuisance regression models. Default is False.",
    )
    nuisance_variables.add_argument(
        "--friston24",
        action="store_true",
        default=False,
        help="Use Friston 24 head motion parameters (x, y, z displacement in mm and associated squares, derivatives of squares, and derivatives) in the nuisance regression model. Default is False.",
    )
    nuisance_variables.add_argument(
        "--globalsignal",
        action="store_true",
        default=False,
        help="Use global signal across brain in nuisance regression model. Default is False.",
    )

    workflow_type = parser.add_argument_group(
        "workflow type", "Flags and options to control overall workflow type."
    )
    workflow_type.add_argument(
        "--fmriprep_noaroma",
        action="store_true",
        help="Post FMRIPREP processing on AROMA denoised 4D image.",
        default=False,
    )
    workflow_type.add_argument(
        "--hcp",
        action="store_true",
        help="HCP preprocessong on HCP minimally preprocessed data.",
    )
    workflow_type.add_argument(
        "--unifiedmodel",
        action="store_true",
        help="Activate computation of seed correlation results with the specified nuisance covariates in the same regression.",
    )

    misc_opts = parser.add_argument_group(
        "misc_args",
        "Miscellaneous other arguments for the cleaning and reporting process.",
    )
    misc_opts.add_argument(
        "--dropvols",
        "-d",
        type=int,
        default=5,
        help="Number of volumes to drop from the scan. Default is 5 volumes.",
    )
    misc_opts.add_argument(
        "--brainmask",
        default="/usr/share/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz",
        help="Path (absolute path) to the brainmask that is desired.",
    )
    misc_opts.add_argument(
        "--smoothing",
        "-s",
        type=int,
        default=0,
        help="Size in mm of kernel used to smooth output data. Set to 0 if no smoothing is desired.",
    )
    misc_opts.add_argument(
        "--susan",
        action="store_true",
        help="Toggle SUSAN pre-smoothing of the data at the kernel set at --smoothing. If smoothing is set to 0 then the default kernel will be a 6mm kernel for SUSAN.",
    )
    misc_opts.add_argument("--seedpaths")
    misc_opts.add_argument("--seed")
    misc_opts.add_argument("--seed_corr_analysis")
    misc_opts.add_argument("--slidingwindow")
    misc_opts.add_argument("--slidingstep")
    misc_opts.add_argument("--working", help="Path to set for working directory.")

    args = parser.parse_args()

    return args


def check_args(args):
    """[summary]
    
    Arguments:
        args {[type]} -- [description]
    
    Returns:
        [type] -- [description]
    """

    # check for existence of input data directory, output data directory
    if os.path.isdir(args.input_directory) is False:
        print(
            f"\nInput data path {args.input_directory} was not a valid directory path."
        )
        sys.exit(1)
    if args.output_directory:
        if os.path.isdir(args.output_directory) is False:
            print(
                f"\nOutput data path {args.output_directory} was not a valid directory path. Creating path now."
            )
            os.makedirs(args.output_directory, exist_ok=True)
    elif not args.output_directory:
        parent_dir = pathlib.Path(args.input_directory).parent
        args.output_directory = str(parent_dir / "nipypreproc")
        print(
            f"\nData output directory was not specified. Data will be written to {args.output_directory}"
        )

    if args.working:
        if os.path.isdir(args.working) is False:
            print(
                f"\nOutput data path {args.output_directory} was not a valid directory path. Creating path now."
            )
            os.makedirs(args.working, exist_ok=True)

    # standardize specific run level options
    if args.subject:
        args.subject = args.subject.replace("sub-", "")
    if args.session:
        args.session = args.session.replace("ses-", "")

    # check if brainmask exists and can be loaded by nibabel
    if os.path.isfile(args.brainmask) is False:
        print(f"Input file path {args.brainmask} was not found, or was not a file.")
        sys.exit(1)
    if utils.check_valid_nii(args.brainmask) is False:
        print(
            f"Provided file {args.brainmask} could not be loaded as a valid NII file."
        )
        sys.exit(1)

    if not args.fmriprep_noaroma and args.smoothing > 0:
        print(
            "FMRIPREP AROMA workflow was desired, but smoothing was already requested. The AROMA workflow provides an already-smoothed image at 6mm smoothing kernel. Pass --smoothing 0 instead to proceed."
        )
        sys.exit(1)

    # make sure the workflow desired is either denoising and post-denoising analysis OR post-denoising analysis
    if args.idfile and (args.hcp or args.fmriprep_aroma or args.fmriprep_nonaroma):
        print(
            "Either one of --hcp, --fmriprep_aroma, --fmriprep_nonaroma, or providing a newline delimited list of paths to .nii/.nii.gz must be provided."
        )
        sys.exit(1)

    if args.seed:
        if not (args.slidingwindow and args.slidingstep) or args.seed_corr_analysis:
            print(
                "If seeds or a list of seeds are provided, either one of sliding window analysis (--slidingwindow, --slidingstep) or --"
            )

    if args.smoothing == 0:
        args.smoothing = False

    if args.nodetrend:
        args.detrend = False
    else:
        args.detrend = True

    if args.nointercept:
        args.intercept = False
    else:
        args.intercept = True

    if args.nobandpass:
        args.lowpass = 0
        args.highpass = 0

    if args.susan and args.smoothing == 0:
        args.smoothing = 6
        print(
            "SUSAN was flagged but default smoothing was set to 0mm. Setting to 6mm by default to be congruent with FMRIPREP workflow."
        )

    if args.susan and not args.fmriprep_noaroma:
        print(
            "You passed --susan but also wanted to use the smoothAROMAnonaggr image from FMRIPREP as the starting point for the preprocessing. Either do not pass --susan or pass --fmriprep_noaroma to use the non AROMA processed image."
        )
        sys.exit(1)

    if args.seedpaths:
        args.seedpaths = utils.read_seed_file_list(args.seedpaths)
    return args


def get_args():
    """
    Parses command line arguments using ArgumentParser and checks them for consistency.
    """
    return check_args(parse_args())


def main(args):
    args = get_args()  # parse, validate args
    print(f"Input data directory is: {args.input_directory}")
    print(f"Output data directory is {args.output_directory}")
    data_object_dict = datasets.get_datadict_from_path(
        args.input_directory, args.subject, args.session
    )  # use bids.BIDSLayout to extract dataclasses with subject data
    if len(data_object_dict) == 0:
        print("There were no subjects found.")
        sys.exit(1)
    preproc.preprocess(data_object_dict, args)


if __name__ == "__main__":
    main(sys.argv[1:])
