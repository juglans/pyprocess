import os
from typing import Union, List, Tuple
import pathlib
import pandas as pd


def read_scan_file_list(scan_file: Union[str, pathlib.Path]):
    scans = []
    with open(scan_file, "r") as f:
        for line in f:
            scans.append(line.strip())
    return scans


def handle_tmpdir():
    if "TMPDIR" in os.environ:
        tmpdir = os.path.join(os.environ["TMPDIR"], "segmentationworkflow-tmpfiles")
    elif os.path.exists("/scratch"):
        tmpdir = "/scratch/segmentationworkflow-tmpfiles"
    else:
        tmpdir = "/tmp"
    return tmpdir


def file_has_header(filepath):
    with open(filepath, "r") as f:
        line = next(f)
    if line[0] == "/":
        return False
    else:
        return True


def read_wmap_csv(scan_file: Union[str, pathlib.Path]):
    has_header = file_has_header(scan_file)
    if has_header:
        df = pd.read_csv(scan_file, header="infer")
    else:
        df = pd.read_csv(scan_file, header=None)
    return df