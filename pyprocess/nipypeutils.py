from nipype import pipeline
from datetime import datetime
import os


def handle_tmpdir():
    if "TMPDIR" in os.environ:
        tmpdir = os.path.join(os.environ["TMPDIR"], "segmentationworkflow-tmpfiles")
    elif os.path.exists("/scratch"):
        tmpdir = "/scratch/segmentationworkflow-tmpfiles"
    else:
        tmpdir = "/tmp"
    return tmpdir


def get_main_workflow():
    timenow = datetime.now()

    tmpdir = handle_tmpdir()
    tmpdir = os.path.join(tmpdir, "segmentationworkflow")
    workflow = pipeline.Workflow(
        name=f"segmentation_workflow-{timenow.year}-{timenow.month}-{timenow.day}-{timenow.hour}-{timenow.second}",
        base_dir=tmpdir,
    )
    workflow.config["execution"] = {"crashfile_format": "txt"}
    return workflow, tmpdir
