import pathlib
import sys
import argparse
import functools
import os
import re
from nipype import pipeline
from nipype.interfaces import fsl, utility, spm, io, matlab
from datetime import datetime
import utils
import numpy as np
import shutil


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_directory")
    parser.add_argument("--filtersubjects")
    parser.add_argument(
        "--input_file", help="Fullpath of a single nii file for segmentation."
    )
    parser.add_argument(
        "--input_filelist",
        help="Fullpath of text files corresponding to the list of files for segmentation.",
    )
    parser.add_argument(
        "--output_directory", help="Fullpath of desired output data directory."
    )
    parser.add_argument("--smoothing", type=int, default=8)

    parser.add_argument("--remove_tmpfiles", type=bool, default=False)
    resource_opts = parser.add_argument_group(
        "resources",
        "Flags and options to control computing resources such as number of cores and amount of memory.",
    )
    resource_opts.add_argument(
        "--n_procs", default=4, type=int, help="Number of processes to use."
    )
    resource_opts.add_argument(
        "--memory_gb",
        default=12,
        type=int,
        help="Amount of memory in GB to use in total.",
    )

    args = parser.parse_args()

    args.output_directory = os.path.abspath(args.output_directory)
    if args.input_file:
        args.input_file = os.path.abspath(args.input_file)
    # need to check for isNone for three input args
    return args


def get_main_workflow():
    timenow = datetime.now()

    tmpdir = handle_tmpdir()
    workflow = pipeline.Workflow(
        name=f"segmentation_workflow-{timenow.year}-{timenow.month}-{timenow.day}-{timenow.hour}-{timenow.second}",
        base_dir=os.path.join(tmpdir, "segmentationworkflow"),
    )
    workflow.config["execution"] = {"crashfile_format": "txt"}
    return workflow, tmpdir


def create_segmentation_analysis(
    input_file, scan_name, session_name, output_directory, base_dir, smoothing_fwhwm=8.0
):

    input_fname = (
        pathlib.Path(input_file).name.replace(".nii.gz", "").replace(".nii", "")
    )

    wkflow_desc_prefix = f"{scan_name}_{session_name}_spmvbm"
    subject_workflow_dir = os.path.join(base_dir, wkflow_desc_prefix)
    subworkflow = pipeline.Workflow(
        name=wkflow_desc_prefix, base_dir=subject_workflow_dir
    )

    subject_anat_dir = os.path.join(output_directory, scan_name, session_name, "anat")
    os.makedirs(subject_anat_dir, exist_ok=True)

    segment = pipeline.Node(
        interface=utils.NewSegment(), name=f"{wkflow_desc_prefix}_segmentation"
    )

    segment.inputs.channel_files = pathlib.Path(input_file)

    spm_base_path = spm.base.Info().path()
    # spm_base_path = "/seeley/imaging/data/mridata/truji/spm12"

    tpmpath = os.path.join(spm_base_path, "tpm", "TPM.nii")
    icv_mask = os.path.join(spm_base_path, "tpm", "mask_ICV.nii")

    regularization_opts = [0, 0.001, 0.5, 0.05, 0.2]
    channel_info = (0.0001, 60, (False, False))

    gm_opts = ((tpmpath, 1), 2, (True, False), (True, True))
    wm_opts = ((tpmpath, 2), 2, (True, False), (True, True))
    csf_opts = ((tpmpath, 3), 2, (True, False), (True, True))
    skull_opts = ((tpmpath, 4), 3, (False, False), (False, False))
    soft_tiss_opts = ((tpmpath, 5), 4, (False, False), (False, False))
    other_opts = ((tpmpath, 6), 2, (False, False), (False, False))

    segment.inputs.tissues = [
        gm_opts,
        wm_opts,
        csf_opts,
        skull_opts,
        soft_tiss_opts,
        other_opts,
    ]

    segment.inputs.write_deformation_fields = [True, True]
    segment.inputs.channel_info = channel_info
    segment.inputs.sampling_distance = 3
    segment.inputs.warping_regularization = regularization_opts
    segment.inputs.affine_regularization = "mni"

    segment.inputs.cleanup = True
    segment.inputs.warping_mrf = True
    segment.inputs.warp_fwhm = 0.0

    output_csplitter = pipeline.Node(
        interface=utility.Function(
            function=utils.separate_spm_segmentations,
            input_names=["segmentation_output"],
            output_names=["gm_path", "wm_path", "csf_path"],
        ),
        name=f"{wkflow_desc_prefix}_native-spm-splitter",
    )

    output_nsplitter = pipeline.Node(
        interface=utility.Function(
            function=utils.separate_spm_segmentations,
            input_names=["segmentation_output"],
            output_names=["gm_path", "wm_path", "csf_path"],
        ),
        name=f"{wkflow_desc_prefix}_unmodulated-spm-splitter",
    )

    output_msplitter = pipeline.Node(
        interface=utility.Function(
            function=utils.separate_spm_segmentations,
            input_names=["segmentation_output"],
            output_names=["gm_path", "wm_path", "csf_path"],
        ),
        name=f"{wkflow_desc_prefix}_modulated-spm-splitter",
    )

    tiv_agg = pipeline.Node(
        interface=utils.TIVCalc(),
        name=f"{wkflow_desc_prefix}_tiv-agg",
    )
    tiv_agg.inputs.mask_image = icv_mask
    tiv_agg.inputs.output_file = os.path.join(
        subject_anat_dir, f"{scan_name}_{session_name}_desc-tiv.csv"
    )

    gmsmooth = pipeline.Node(
        interface=spm.Smooth(), name=f"{wkflow_desc_prefix}_gmsmooth"
    )
    wmsmooth = pipeline.Node(
        interface=spm.Smooth(), name=f"{wkflow_desc_prefix}_wmsmooth"
    )
    csfsmooth = pipeline.Node(
        interface=spm.Smooth(), name=f"{wkflow_desc_prefix}_csfsmooth"
    )

    for smoother in (gmsmooth, wmsmooth, csfsmooth):
        smoother.inputs.fwhm = [8.0, 8.0, 8.0]

    c1_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_c1-export"
    )
    c2_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_c2-export"
    )
    c3_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_c3-export"
    )

    mwc1_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_mwc1-export"
    )
    mwc2_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_mwc2-export"
    )
    mwc3_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_mwc3-export"
    )

    smwc1_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_smwc1-export"
    )
    smwc2_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_smwc2-export"
    )
    smwc3_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_smwc3-export"
    )
    tiv_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_tiv-export"
    )
    tiv_export.inputs.out_file = os.path.join(
        subject_anat_dir, f"{scan_name}_{scan_name}_desc-tiv.txt"
    )
    tiv_export.inputs.clobber = True

    forward_def_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_fwddeformation-export"
    )

    backward_def_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_bkwddeformation-export"
    )

    transformation_mat_export = pipeline.Node(
        interface=io.ExportFile(), name=f"{wkflow_desc_prefix}_tfmmat-export"
    )
    transformation_mat_export.inputs.out_file = os.path.join(
        subject_anat_dir, f"{scan_name}_{session_name}_desc-tfmat.mat"
    )
    transformation_mat_export.inputs.clobber = True

    for exporter in (
        mwc1_export,
        mwc2_export,
        mwc3_export,
        smwc1_export,
        smwc2_export,
        smwc3_export,
        c1_export,
        c2_export,
        c3_export,
        forward_def_export,
        backward_def_export,
    ):

        search = re.search("ses-\d+_(.*)-export", exporter.name)
        if search:
            output_name = search.group(1)

        new_filename = f"{scan_name}_{session_name}_desc-{output_name}.nii"
        export_fullfile_path = os.path.join(subject_anat_dir, new_filename)
        exporter.inputs.out_file = export_fullfile_path
        exporter.inputs.clobber = True

    subworkflow.connect(
        [
            (
                segment,
                output_msplitter,
                [("modulated_class_images", "segmentation_output")],
            ),
            (
                segment,
                output_nsplitter,
                [("normalized_class_images", "segmentation_output")],
            ),
            (
                segment,
                output_csplitter,
                [("native_class_images", "segmentation_output")],
            ),
            (output_msplitter, gmsmooth, [("gm_path", "in_files")]),
            (output_msplitter, wmsmooth, [("wm_path", "in_files")]),
            (output_msplitter, csfsmooth, [("csf_path", "in_files")]),
            (output_msplitter, tiv_agg, [("gm_path", "mwc1_image")]),
            (output_msplitter, tiv_agg, [("wm_path", "mwc2_image")]),
            (output_msplitter, tiv_agg, [("csf_path", "mwc3_image")]),
            (gmsmooth, smwc1_export, [("smoothed_files", "in_file")]),
            (wmsmooth, smwc2_export, [("smoothed_files", "in_file")]),
            (csfsmooth, smwc3_export, [("smoothed_files", "in_file")]),
            (output_msplitter, mwc1_export, [("gm_path", "in_file")]),
            (output_msplitter, mwc2_export, [("wm_path", "in_file")]),
            (output_msplitter, mwc3_export, [("csf_path", "in_file")]),
            (output_csplitter, c1_export, [("csf_path", "in_file")]),
            (output_csplitter, c2_export, [("csf_path", "in_file")]),
            (output_csplitter, c3_export, [("csf_path", "in_file")]),
            (segment, forward_def_export, [("forward_deformation_field", "in_file")]),
            (segment, backward_def_export, [("inverse_deformation_field", "in_file")]),
            (segment, transformation_mat_export, [("transformation_mat", "in_file")]),
        ]
    )

    # get normalized_class_images; transformation_mat from the segmentation output and route them into a datasink; maybe manually change paths, this change path portion cannot be done from the spm node directly
    # use exportnode to get individual files
    # TODO: write a node that will take the output_modulated_images paths from GM, CSF, and then rename them into the output of the desired output directory
    # one node will need to smooth the mwc1 images as well, not sure how to do this. maybe we can just use an iternode, but im not sure how to deal with the None elements. maybe
    # a separate function that slices the list, like in _btthresh function ; to another node that takes the various inputs and also an output dir, subject name?
    # ie one fn take the list, slices it into pieces iwth one output as gm_path, another csf_path, wm_path, etc; maybe this could be one fn called rename_gm_outputs
    # and another rename_wm_outputs and moves them directly to the output dir, passing this path and the desired smoothed path to a third smoothing fn?
    # somehow we will need to pass the output directory to this new node, maybe the inputs will be the list of output paths as separate inputs and then the desired output dir set manually

    return subworkflow


def handle_tmpdir():
    if "TMPDIR" in os.environ:
        tmpdir = os.path.join(os.environ["TMPDIR"], "segmentationworkflow-tmpfiles")
    elif os.path.exists("/scratch"):
        tmpdir = "/scratch/segmentationworkflow-tmpfiles"
    else:
        tmpdir = "/tmp"
    return tmpdir


def segmentation_workflow(
    input_files,
    output_directory,
    n_procs=1,
    memory_gb=10,
    smoothing_fwhm=0.0,
):

    workflow, tmpdir = get_main_workflow()
    print("Starting workflow setup.")
    workflow_nodes = []
    for scan_index, scan_path in enumerate(input_files):
        scan_name = (
            os.path.basename(scan_path).replace(".nii.gz", "").replace(".nii", "")
        )
        scan_search = re.search("(sub-\w+)_.*", scan_path)
        session_search = re.search("(ses-\d+)", scan_path)

        if scan_search is None:
            print(
                f"Scan {scan_search}; could not find PIDN. Please add a subject string with sub-**** to the filename so that we can index it and save it properly."
            )
            sys.exit(1)

        if session_search is None:
            print(
                f"Scan {scan_name}; could not find session. Please add a subject string with ses-**** to the filename so that we can index it and save it properly."
            )
            sys.exit(1)

        subject_name = scan_search.group(1)
        session_name = session_search.group(1)

        static_seed_nodes = create_segmentation_analysis(
            scan_path, subject_name, session_name, output_directory, workflow.base_dir
        )
        print(f"Finished workflow setup for: {subject_name}, {session_name}.")

        workflow_nodes.append(static_seed_nodes)
    workflow.add_nodes(workflow_nodes)

    workflow.run(
        plugin="MultiProc",
        plugin_args={
            "n_procs": n_procs,
            "memory_gb": memory_gb,
            "crashfile_format": "txt",
        },
    )
    return tmpdir


def read_scan_file_list(scan_file):
    scans = []
    with open(scan_file, "r") as f:
        for line in f:
            scans.append(line.strip())
    return scans


def check_convert_gunzip(scan_path):
    if scan_file.endswith(".nii.gz"):
        pass


if __name__ == "__main__":

    #    matlab.MatlabCommand.set_default_paths("/seeley/imaging/data/mridata/truji/spm12")
    args = parse_args()
    if args.input_filelist:
        scan_file_list = read_scan_file_list(args.input_filelist)
    elif args.input_file:
        scan_file_list = [args.input_file]

    tmpdir = segmentation_workflow(
        scan_file_list,
        args.output_directory,
        n_procs=args.n_procs,
        memory_gb=args.memory_gb,
        smoothing_fwhm=args.smoothing,
    )

    if args.remove_tmpfiles:
        shutil.rmtree(tmpdir)