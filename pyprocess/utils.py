import nibabel as nib
import numpy as np
import pandas as pd
import os
import traits
from nipype.interfaces import afni, base, matlab, spm
from nipype.interfaces.base import traits
from nipype import utils
from nipype.external import due
import copy


def read_seed_file_list(seed_file):
    seed_files = []
    with open(seed_file, "r") as f:
        for line in f:
            seed_files.append(line.strip())
    seed_files = [line for line in seed_files if line is not None]
    return seed_files


def check_valid_nii(nii_path):
    """
    Tries to load a string path using nibabel's load function.
    Arguments:
        nii_path {[str]} -- String path to attempt loading as nifti image using nibabel.load()

    Returns:
        [boolean] -- [description
    """

    assert type(nii_path) == str

    endswith_nii = nii_path.endswith(".nii") or nii_path.endswith(".nii.gz")
    if not endswith_nii:
        print(
            f"Tried to use nibabel load to load {nii_path} but the string ending is not .nii or .nii.gz."
        )
        print(nii_path.endswith(".nii.gz"))
        print(nii_path.endswith(".nii"))
        return False

    try:
        nib.load(nii_path)
        return True
    except TypeError:
        print(f"Could not load file: {nii_path}.")
        return False


def extract_tr(nii):
    """
    Extracts TR from nii file from header field pixdim4.
    Arguments:
        nii [str or nibabel.nifti1.Nifti1Image] -- String path to nii file or nibabel.nifti1.Nifti1Image object.

    Returns:
        [int] -- TR in seconds.
    """

    if type(nii) == nib.nifti1.Nifti1Image:
        return nii.header["pixdim"][4]
    elif type(nii) is str:
        assert check_valid_nii(nii) is True

        nii_obj = nib.load(nii)
        return nii_obj.header["pixdim"][4]


def is_binary_array(nparray):
    """
    Utility fn to return a Boolean indicating whether a numpy array is only composed of 1's and 0's.

    Arguments:
        nparray {[numpy.ndarray]} -- numpy array to check if is binary.

    Returns:
        [bool] -- True if all array elements are either 0 or 1, False if not.
    """

    assert isinstance(nparray, np.ndarray)
    return ((nparray == 0) | nparray == 1).all()


def convert_to_binary(nparray):
    """
    Utility fn to binarize (set all values greater than 0 to equal 1) a numpy array.

    Arguments:
        nparray {[numpy.ndarray]} -- numpy array to binarize.

    Returns:
        [numpy.ndarray] -- numpy array with all values either 0 or 1.
    """

    assert isinstance(nparray, np.ndarray)
    binarized_array = nparray.copy()
    binarized_array[binarized_array > 0] = 1

    return binarized_array


def compute_fdjenk(df):
    brain_radius = 50  # mm

    motion_parameters = df[
        ["trans_x", "trans_y", "trans_z", "rot_x", "rot_y", "rot_z"]
    ].values
    motion_parameters[..., 3:] = motion_parameters[..., 3:] * brain_radius

    fd_jenk = np.sqrt(
        np.square(motion_parameters[1:, :] - motion_parameters[:-1, :]).sum(axis=1)
    )
    fd_jenk = np.insert(fd_jenk, 0, 0)
    return fd_jenk


def prepare_confounds_file(
    confound_filepath,
    dropvols,
    intercept=True,
    detrend=True,
    acompcor=False,
    tcompcor=False,
    tissue_signals=False,
    friston24=False,
    global_signal=False,
    mode="fmriprep",
):
    from pandas import read_csv
    from numpy import savetxt, sqrt, square, insert
    from utils import compute_fdjenk
    import os

    # def compute_fdjenk(df):
    #    brain_radius = 50  # mm

    #    motion_parameters = df[
    #        ["trans_x", "trans_y", "trans_z", "rot_x", "rot_y", "rot_z"]
    #    ].values
    #    motion_parameters[..., 3:] = motion_parameters[..., 3:] * brain_radius

    #    fd_jenk = sqrt(
    #        square(motion_parameters[1:, :] - motion_parameters[:-1, 0]).sum(axis=1)
    #    )
    #    fd_jenk = insert(fd_jenk, 0, 0)
    #    return fd_jenk

    print("MODE VARIABLE IS ", mode)
    if mode == "fmriprep":
        confounds_df = read_csv(confound_filepath, sep="\t")
        confounds_df = confounds_df.fillna(0)
        confounds_df["fd_jenk"] = compute_fdjenk(confounds_df)

        confounds_to_use = []

        friston24_cols = [
            c
            for c in confounds_df.columns
            if "rot" in c.lower() or "trans" in c.lower()
        ]
        if friston24:
            print("Using Friston 24 as nuisance regressors.")
            confounds_to_use += friston24_cols
        if acompcor or tcompcor:
            print("Using CompCors for nuisance regressors.")
            confounds_to_use += [
                c for c in confounds_df.columns if "cosine" in c.lower()
            ]
        if acompcor:
            print("Using aCompCor.")
            confounds_to_use += [
                c for c in confounds_df.columns if "a_comp" in c.lower()
            ]
        if tcompcor:
            print("Using tCompCor.")
            confounds_to_use += [
                c for c in confounds_df.columns if "t_comp" in c.lower()
            ]
        if tissue_signals:
            print("Using mean tissue signals from eroded segmentation.")
            wm_csf_signals = [
                c
                for c in confounds_df.columns
                if "csf" in c.lower() or "white_matter" in c.lower()
            ]
            wm_csf_signals = [
                c
                for c in wm_csf_signals
                if "square" not in c and "deriv" not in c and "power" not in c
            ]
            confounds_to_use += wm_csf_signals
        if global_signal:
            print("Using global signal.")
            confounds_to_use.append("global_signal")

        confounds_filtered = confounds_df[confounds_to_use][dropvols:]

        motion_param_columns = [
            c for c in friston24_cols if "deriv" not in c and "power" not in c
        ]

        motion_param_columns.append("dvars")
        motion_param_columns.append("framewise_displacement")
        motion_params = confounds_df[motion_param_columns].rename(
            columns={"framewise_displacement": "fd_power"}
        )
        motion_params["fd_jenk"] = compute_fdjenk(confounds_df)

    elif mode == "hcp":
        raise NotImplementedError
    else:
        print(
            'The "mode" variable for confounds extraction must be one of "fmriprep" or "hcp".'
        )
        return None, None, None

    if (
        detrend
    ):  # TODO Remove: this was taken care with another fn to add intercept / detrending terms after bandpassing
        confounds_filtered["linear_trend"] = list(
            range(1, confounds_filtered.shape[0] + 1)
        )

    filepath_split = os.path.basename(confound_filepath).split(".")
    filepath_stem = "".join(filepath_split[:-1]).split("desc")
    filepath_stem = "".join(filepath_stem[:1]) + "desc"

    out_txtfile_path = os.path.join(os.getcwd(), f"{filepath_stem}_confounds.txt")
    out_csvfile_path = os.path.join(os.getcwd(), f"{filepath_stem}_confounds.csv")
    motion_param_csvfile_path = os.path.join(os.getcwd(), f"{filepath_stem}-motion.csv")

    savetxt(out_txtfile_path, confounds_filtered.values)
    confounds_filtered.to_csv(out_csvfile_path, index=False)
    motion_params.to_csv(motion_param_csvfile_path, index=False)
    return out_txtfile_path, out_csvfile_path, motion_param_csvfile_path


def add_intercept(seed_tseries_path):
    from numpy import genfromtxt, savetxt, c_, arange, ones
    from os.path import basename, dirname
    from os.path import join as opjoin
    from os import getcwd

    txt_array = genfromtxt(seed_tseries_path)
    num_rows = txt_array.shape[0]

    intercept_col = ones(num_rows)
    txt_array = c_[txt_array, intercept_col]

    filepath_split = basename(seed_tseries_path).split(".")
    filepath_stem = "".join(filepath_split[:-1]).split("desc")
    filepath_stem = "".join(filepath_stem[:1]) + "desc"

    out_txtfile_path = opjoin(
        dirname(seed_tseries_path), f"{filepath_stem}-confounds_bold.txt"
    )

    savetxt(out_txtfile_path, txt_array)
    return out_txtfile_path


def demean_and_add_regressors(
    txt_path,
    csv_path,
    add_linear_term=False,
    add_intercept_term=False,
    seed_tseries_file=None,
    seed_name=None,
):
    from numpy import genfromtxt, savetxt, c_, arange, ones
    import os
    from pandas import read_csv

    if seed_name is None:
        seed_name = ""
    txt_array = genfromtxt(txt_path)
    csv_as_df = read_csv(csv_path)
    demeaned_txt_array = txt_array + csv_as_df.values.mean(
        axis=0
    )  # - txt_array.mean(axis=0)

    csv_as_df = read_csv(csv_path)
    csv_as_df[:] = demeaned_txt_array

    demeaned_csv_as_df = csv_as_df  # - csv_as_df.mean(axis=0)

    num_rows = len(txt_array)
    if add_linear_term:
        linear_term = arange(1, num_rows + 1)
        demeaned_txt_array = c_[demeaned_txt_array, linear_term]
        demeaned_csv_as_df["linear_trend"] = linear_term
        print(
            f"Appending linear term to design. Shape of design matrix was: {txt_array.shape}, now {demeaned_txt_array.shape}."
        )

    if add_intercept_term:
        intercept_term = ones(num_rows)
        demeaned_txt_array = c_[demeaned_txt_array, intercept_term]
        demeaned_csv_as_df["intercept_term"] = intercept_term
        print(
            f"Appending intercept term to design. Shape of design matrix was: {txt_array.shape}, now {demeaned_txt_array.shape}."
        )

    if seed_tseries_file:
        print(f"Appending seed timeseries.")
        seed_timeseries = genfromtxt(seed_tseries_file)
        # seed_timeseries = seed_timeseries - seed_timeseries.mean()

        demeaned_txt_array = c_[seed_timeseries, demeaned_txt_array]
        demeaned_csv_as_df["seed_timeseries"] = seed_timeseries
        print(
            f"Appending seed timeseries term to design. Shape of design matrix was: {txt_array.shape}, now {demeaned_txt_array.shape}."
        )

    filepath_split = os.path.basename(csv_path).split(".")
    filepath_stem = "".join(filepath_split[:-1]).split("desc")
    filepath_stem = "".join(filepath_stem[:1]) + "desc"

    if seed_tseries_file:
        seed_basename = os.path.basename(seed_tseries_file).replace(".txt", "")
        out_txtfile_path = os.path.join(
            os.getcwd(), f"{filepath_stem}_{seed_basename}-confounds_bold.txt"
        )
        out_csvfile_path = os.path.join(
            os.getcwd(), f"{filepath_stem}_{seed_basename}-confounds_bold.csv"
        )
    else:
        if seed_name == "":
            out_txtfile_path = os.path.join(
                os.getcwd(), f"{filepath_stem}-cleanconfounds_bold.txt"
            )
            out_csvfile_path = os.path.join(
                os.getcwd(), f"{filepath_stem}-cleanconfounds_bold.csv"
            )
        else:
            out_txtfile_path = os.path.join(
                os.getcwd(), f"{filepath_stem}-cleanconfounds_seed_{seed_name}_bold.txt"
            )
            out_csvfile_path = os.path.join(
                os.getcwd(), f"{filepath_stem}-cleanconfounds_seed_{seed_name}_bold.csv"
            )

    savetxt(out_txtfile_path, demeaned_txt_array)
    demeaned_csv_as_df.to_csv(out_csvfile_path, index=False)
    return out_txtfile_path, out_csvfile_path


def remean_image(demeaned_image, image_with_mean):
    from nibabel import load, Nifti1Image
    import os

    demeaned_data_nifti = load(demeaned_image)
    demeaned_data = demeaned_data_nifti.get_fdata()

    pre_unmean_nifti = load(image_with_mean)
    mean_image = pre_unmean_nifti.get_fdata()  # .mean(axis=-1)
    # dims for nibabel are x y z t; so we can just average over the t dimension

    remeaned_data = demeaned_data + mean_image[..., None]
    demeaned_image_basename = os.path.basename(demeaned_image)
    demeaned_image_base_stem = demeaned_image_basename.replace(".nii.gz", "").replace(
        ".nii", ""
    )

    output_basename = f"{demeaned_image_base_stem}_remeaned.nii.gz"
    output_fpath = os.path.join(os.getcwd(), output_basename)

    output_nii_object = Nifti1Image(
        remeaned_data, demeaned_data_nifti.affine, demeaned_data_nifti.header
    )
    output_nii_object.to_filename(output_fpath)
    return output_fpath


def write_denoising_desc_json(json_dict, desc_string, target_filename):
    from json import dump
    from re import sub
    import os

    print(desc_string, target_filename)
    print("----------")
    output_fpath = sub(
        "space.*",
        desc_string,
        os.path.join(os.path.basename(target_filename), "space.*", desc_string),
    )
    output_fpath = os.path.join(os.getcwd(), output_fpath + ".json")
    print(output_fpath)
    print("-----------")
    with open(output_fpath, "w") as f:
        dump(json_dict, f, indent=4)

    return output_fpath


def get_tiv(matfile):
    from scipy import io
    import os

    mat_data = io.loadmat(matfile, squeeze_me=True)
    tiv_volumes = (
        mat_data["volumes"].item()[0].tolist()
    )  # returns 1x3 with GM/WM/CSF as vol in L
    total_vol = sum(tiv_volumes)
    tiv_volumes.extend([total_vol])

    with open(os.path.join(os.getcwd(), "tiv.txt"), "w") as f:
        f.write("gm_vol, wm_vol, csf_vol, total_vol\n")
        f.write(",".join(tiv_volumes))

    return "tiv.txt"


def separate_spm_segmentations(segmentation_output):
    # spm NewSegment modulated_outputs or unmodulated_outputs are a list of lists
    # for each tissue category there will be a list with one item, so we need to unnest twice

    gm_path, wm_path, csf_path, _, _, _ = segmentation_output
    return gm_path[0], wm_path[0], csf_path[0]


def _get_btthresh(median_value):
    return 0.75 * median_value


def _get_usans(image, threshold_value):
    return [tuple([image, threshold_value])]


def create_movement_plots(movement_csv, dirty_snr_map=None, clean_snr_map=None):
    from pandas import read_csv
    import os
    import seaborn as sns
    import matplotlib.pyplot as plt
    from matplotlib import ticker
    from nilearn import plotting

    # probably needs gridpspec. demo in ipython first
    # xyz coords could be 0 0 6 and 0 -77 6
    # maybe -39 -18 6 and 39 18 6

    plt.style.use("dark_background")

    df = read_csv(movement_csv)
    df = df.reset_index().rename(columns={"index": "TR"})

    assert df.ndim == 2

    num_trs, _ = df.shape
    plot_xlim = num_trs + 5

    rotation_cols = ["rot_x", "rot_y", "rot_z"]
    translation_cols = ["trans_x", "trans_y", "trans_z"]
    movement_plotnames = ["x-axis", "y-axis", "z-axis"]
    framewise_colname = "fd_jenk"
    dvars_colname = "dvars"

    fig, axs = plt.subplots(4, 1, figsize=(10, 10))

    for idx, col in enumerate(translation_cols):
        sns.lineplot(
            x=df["TR"], y=df[col].abs(), label=movement_plotnames[idx], ax=axs[0]
        )
    # axs[0].axhline(3, ls="--", label="0.3 mm translation cut-off", color="r")
    axs[0].set_title(f"Translation (mm) over run, num. TRs = {num_trs}")
    axs[0].legend(bbox_to_anchor=(1.025, 1), loc=2)
    axs[0].set_xlabel("TR index")
    axs[0].set_ylabel("Translation (mm)")
    axs[0].set_xlim(0, plot_xlim)

    for idx, col in enumerate(rotation_cols):
        sns.lineplot(
            x=df["TR"],
            y=df[col].abs(),
            label=movement_plotnames[idx],
            ax=axs[1],
            size=1,
        )

    # axs[1].axhline(
    #    0.0523599, ls="--", label="3 degrees (0.052 rad) rotation cut-off", color="r"
    # )

    axs[1].set_title(f"Rotation (rad.) over run, num. TRs = {num_trs}")
    axs[1].legend(bbox_to_anchor=(1.025, 1), loc=2)
    axs[1].set_xlabel("TR index")
    axs[1].set_ylabel("Rotation (Radians)")
    axs[1].set_xlim(0, plot_xlim)

    mean_framewise = df[framewise_colname].mean()
    sns.lineplot(
        x=df["TR"],
        y=df[framewise_colname],
        label="$Framewise displacement (FD_{Jenkinson})$",
        ax=axs[2],
        size=1,
    )

    axs[2].axhline(0.25, ls="--", label="0.25 mm FD", color="gold")
    axs[2].axhline(0.55, ls="-", label="0.55 mm FD", color="r")
    axs[2].set_xlabel("TR index")
    axs[2].set_ylabel("Displacement (mm)")
    axs[2].set_title(r"$Framewise displacement FD_{Jenkinson}$")
    axs[2].set_xlim(0, plot_xlim)
    axs[2].legend(bbox_to_anchor=(1.025, 1), loc=2)

    sns.lineplot(x=df["TR"], y=df[dvars_colname], label="DVARS", ax=axs[3])
    axs[3].set_xlabel("TR index")
    axs[3].set_ylabel("RMS differences")
    axs[3].set_title(f"DVARS over run, num. TRs = {num_trs}")
    axs[3].set_xlim(0, plot_xlim)
    axs[3].legend(bbox_to_anchor=(1.025, 1), loc=2)

    # need to make the nilearn brain plots separately, unfortunately
    # next plots will be of tsnr dirty/clean for 2 pairs of xyz cut; 4 total; need to make sure to set equal vmin/vmax with some heuristic
    # plotting.plot_glass_brain(dirty_snr_map, title='tSNR of untransformed data',
    #        annotate=True, black_bg=True, display_mode='lzr', axes=axs[4], threshold=2,
    #        colorbar=True)#, vmin=10, vmax=75)
    # axs[4].set_ylabel('tSNR (dirty)')
    #
    # plotting.plot_glass_brain(clean_snr_map, title='tSNR of cleaned data',
    #        annotate=True, black_bg=True, display_mode='lzr', axes=axs[5], threshold=2,
    #        colorbar=True)#, vmin=10, vmax=75)

    plt.subplots_adjust(hspace=0.7)

    out_fname_root = os.path.basename(movement_csv).split("desc")[0]
    timeseries_plot_name = os.path.join(os.getcwd(), out_fname_root + "desc-qcplot.png")
    dirty_snr_plot_name = os.path.join(
        os.getcwd(), out_fname_root + "desc-dirtysnr.png"
    )
    clean_snr_plot_name = os.path.join(
        os.getcwd(), out_fname_root + "desc-cleansnr.png"
    )

    fig.savefig(timeseries_plot_name, transparent=False, bbox_inches="tight")

    plt.style.use("classic")
    plt.rc("ytick", labelsize=10)

    if dirty_snr_map:
        plt.figure()
        plotting.plot_img(
            dirty_snr_map,
            title="tSNR of untransformed data",
            annotate=True,
            black_bg=False,
            cut_coords=(-55, -50, -30, -10, 0, 10, 20, 30, 50, 60),
            display_mode="z",
            colorbar=True,
            cmap="viridis",
        )
        # for ax in plt.gcf().axes: ax.yaxis.set_major_formatter(ticker.FormatStrFormatter("%.2f"))

        plt.savefig(dirty_snr_plot_name, transparent=False)

    if clean_snr_map:
        plt.figure()
        plotting.plot_img(
            clean_snr_map,
            title="tSNR of cleaned data",
            annotate=True,
            black_bg=False,
            cut_coords=(-55, -50, -30, -10, 0, 10, 20, 30, 50, 60),
            display_mode="z",
            colorbar=True,
            cmap="viridis",
        )
        # for ax in plt.gcf().axes: ax.yaxis.set_major_formatter(ticker.FormatStrFormatter("%.2f"))

        plt.savefig(clean_snr_plot_name, transparent=False)

    return timeseries_plot_name, dirty_snr_plot_name, clean_snr_plot_name


def write_motion_summary_to_json(confounds_df, json_dict, tr, dropvols=0):
    # stringent / lenient mFD_Jenksin criterion +
    # 5 mm absolute spike threshold derived from https://doi.org/10.1016/j.neuroimage.2017.12.073 (Parkes et al. 2018 [NeuroImage])
    # 4 minutes of good data requirement taken from https://doi.org/10.1016/j.neuroimage.2012.08.052 (Satterwaithe et al. 2013 [NeuroImage])

    df = pd.read_csv(confounds_df, sep="\t")

    lenient_mean_framewise_thresh = (
        0.55  # if tr == 2 else 0.10625  # "lenient" overall mean FD threshold
    )
    stringent_mean_framewise_thresh = (
        0.25
        # if tr == 2
        # else 0.23375  # assume if tr != 2 that tr == 0.85 for prisma. this can definitely be coded...
        # "stringent" overall mean FD threshold
    )

    stringent_framewise_thresh = 0.2

    spike_threshold = 5

    # minimum 4 minutes threshold comes from: https://10.1016/j.neuroimage.2012.08.052 (Satterthwaite et al. 2014 [NeuroImage])
    min_number_minutes = 4
    frames_for_min_time = int(np.rint((min_number_minutes * 60) / tr))

    framewise_disp_tseries = compute_fdjenk(df)[dropvols:]

    tot_num_frames = df.shape[0]
    tot_num_frames_after_dropvols = len(framewise_disp_tseries)

    total_run_length = int(np.rint(tot_num_frames_after_dropvols * tr))

    json_dict["Run length (min)"] = str(int(np.rint(total_run_length / 60.0)))
    json_dict["Total number volumes"] = str(tot_num_frames_after_dropvols)

    mean_framewise = np.round(framewise_disp_tseries.mean(), 2)

    # if any frames above 5 mm, possibly exclude
    any_frames_above_spike_threshold = (
        framewise_disp_tseries > spike_threshold
    ).sum() > 0

    twenty_pct_above_fd_cutoff = (
        framewise_disp_tseries > stringent_framewise_thresh
    ).sum() / total_run_length > total_run_length * 0.2

    passes_lenient_mean_thresh = (
        mean_framewise < lenient_mean_framewise_thresh
    )  # check if mean FD > "lenient" thresh

    passes_stringent_mean_thresh = (
        mean_framewise < stringent_mean_framewise_thresh
    )  # check if mean FD > "stringent" thresh

    num_frames_below_lenient_mean_thresh = (
        framewise_disp_tseries < lenient_mean_framewise_thresh
    ).sum()

    pct_frames_below_lenient_mean_thresh = int(
        100
        * np.rint(num_frames_below_lenient_mean_thresh / tot_num_frames_after_dropvols)
    )

    # compute # and % of frames that are above stringent thresh
    num_frames_below_stringent_mean_thresh = (framewise_disp_tseries < 0.2).sum()

    pct_frames_below_stringent_mean_thresh = int(
        np.rint(
            100 * num_frames_below_stringent_mean_thresh / tot_num_frames_after_dropvols
        )
    )

    twenty_pct_above_fd_cutoff = pct_frames_below_stringent_mean_thresh > 80

    number_minutes_below_lenient = np.round(
        (num_frames_below_lenient_mean_thresh * tr / 60.0), 2
    )

    number_minutes_below_stringent = np.round(
        (num_frames_below_stringent_mean_thresh * tr / 60.0), 2
    )

    json_dict["Motion summary"] = {
        "Scan mean FD (Jenkinson)": mean_framewise,
        "Desired number of minutes of low motion data": str(min_number_minutes),
        "Lenient FD (Jenkinson) threshold (mm)": str(lenient_mean_framewise_thresh),
        "Stringent FD (Jenkinson) threshold (mm)": str(stringent_mean_framewise_thresh),
    }

    json_dict["Motion summary"]["Spike information"] = {
        "Catastrophic motion spike threshold value (mm)": str(spike_threshold),
        "Scan has catastrophic motion spike": str(any_frames_above_spike_threshold),
    }

    json_dict["Motion summary"]["Lenient motion criteria"] = {
        "Threshold value (mm)": str(lenient_mean_framewise_thresh) + " mm",
        "Passes motion and time criteria": str(passes_lenient_mean_thresh),
        "Passes mean framewise criterion": str(passes_lenient_mean_thresh),
        "Frames below threshold": str(num_frames_below_lenient_mean_thresh),
        "Minutes below threshold": str(number_minutes_below_lenient),
        "Percentage of frames below threshold": str(
            pct_frames_below_lenient_mean_thresh
        ),
    }

    json_dict["Motion summary"]["Stringent motion criteria"] = {
        "Threshold value (mm)": str(stringent_mean_framewise_thresh) + " mm",
        "Passes motion and time criteria": str(
            passes_stringent_mean_thresh
            and (not any_frames_above_spike_threshold)
            and twenty_pct_above_fd_cutoff
        ),
        "Passes mean framewise criterion": str(passes_stringent_mean_thresh),
        "Passes single spike size criterion": str(not any_frames_above_spike_threshold),
        r"Passes 80% less than 0.2mm criterion": str(twenty_pct_above_fd_cutoff),
        "Frames below threshold": str(num_frames_below_stringent_mean_thresh),
        "Minutes below threshold": str(number_minutes_below_stringent),
        "Percentage of frames below threshold": str(
            pct_frames_below_stringent_mean_thresh
        ),
    }
    return json_dict


class BP1D_InputSpec(afni.base.AFNICommandInputSpec):
    in_file = base.File(
        desc="input file to bandpass",
        argstr="%s",
        position=-2,
        mandatory=True,
        exists=True,
        copyfile=True,
    )

    lowpass = base.traits.Float(
        desc="lowpass", argstr="%f", position=-3, mandatory=True
    )

    highpass = base.traits.Float(
        desc="highpass", argstr="%f", position=-4, mandatory=True
    )

    dt = base.traits.Float(desc="TR", argstr="-dt %f", position=-5, mandatory=True)

    no_detrend = base.traits.Bool(
        argstr="-nodetrend",
        mandatory=True,
        position=-6,
        desc="Skip the quadratic detrending that occurs before the FFT-based bandpassing",
    )

    out_file = base.File(
        desc="Output file from 1dBandpass",
        argstr="> %s",
        position=-1,
        name_source="in_file",
        name_template="%s_bp",
        keep_extension="False",
    )


class BP1D_OutputSpec(base.TraitedSpec):
    out_file = base.File(exists=True, desc="output_file")


class OneDBandpass(afni.base.AFNICommand):
    _cmd = "1dBandpass"
    input_spec = BP1D_InputSpec
    output_spec = BP1D_OutputSpec


class TIVInputSpec(base.BaseInterfaceInputSpec):
    mwc1_image = base.File(
        desc="Image path to mwc1 (GM) nifti file.", exists=True, mandatory=True
    )

    mwc2_image = base.File(
        desc="Image path  to mwc2 (WM) nifti file.", exists=True, mandatory=True
    )

    mwc3_image = base.File(
        desc="Image path to mwc3 (WM) nifti file.", exists=True, mandatory=True
    )

    mask_image = base.File(desc="Image path to mask.", exists=True, mandatory=True)

    output_file = base.Str(desc="Output file.", mandatory=True)

    script = base.Str(desc="Script text.")


class TIVOutputSpec(base.TraitedSpec):
    output_file = base.File(desc="Output text file.", exists=True)
    stdout = base.Str(desc="stdout")


class TIVCalc(base.BaseInterface):
    input_spec = TIVInputSpec
    output_spec = TIVOutputSpec

    def _tiv_script(self):
        script = rf"""
        gm_vol = spm_vol('{self.inputs.mwc1_image}');
        wm_vol = spm_vol('{self.inputs.mwc2_image}');
        csf_vol = spm_vol('{self.inputs.mwc3_image}');

        mask = spm_vol('{self.inputs.mask_image}');
        
        gm_tiv = spm_summarise(gm_vol, mask, 'litres');
        wm_tiv = spm_summarise(wm_vol, mask, 'litres');
        csf_tiv = spm_summarise(csf_vol, mask, 'litres');
        total_tiv = gm_tiv + wm_tiv + csf_tiv
        
        out = sprintf('%f,%f,%f,%f', gm_tiv, wm_tiv, csf_tiv, total_tiv);
        fid = fopen('{self.inputs.output_file}', 'wt');
        fprintf(fid, 'gm,wm,csf,total_tiv\n');
        fprintf(fid, out);
        fclose(fid);
        exit;
        """
        return script

    def _run_interface(self, runtime):
        self.inputs.script = self._tiv_script()
        matlab_cmd = matlab.MatlabCommand(script=self.inputs.script, mfile=True)
        result = matlab_cmd.run()
        return result.runtime

    def _list_outputs(self):

        outputs = self._outputs().get()
        outputs["output_file"] = os.path.abspath(self.inputs.output_file)
        return outputs


class NewSegmentInputSpec(spm.base.SPMCommandInputSpec):
    channel_files = base.InputMultiPath(
        base.File(exists=True),
        desc="A list of files to be segmented",
        field="channel",
        copyfile=False,
        mandatory=True,
    )

    channel_info = traits.Tuple(
        traits.Float(),
        traits.Float(),
        traits.Tuple(traits.Bool, traits.Bool),
        desc="""A tuple with the following fields:
            - bias reguralisation (0-10)
            - FWHM of Gaussian smoothness of bias
            - which maps to save (Corrected, Field) - a tuple of two boolean values""",
        field="channel",
    )
    tissues = traits.List(
        traits.Tuple(
            traits.Tuple(base.File(exists=True), traits.Int()),
            traits.Int(),
            traits.Tuple(traits.Bool, traits.Bool),
            traits.Tuple(traits.Bool, traits.Bool),
        ),
        desc="""A list of tuples (one per tissue) with the following fields:
            - tissue probability map (4D), 1-based index to frame
            - number of gaussians
            - which maps to save [Native, DARTEL] - a tuple of two boolean values
            - which maps to save [Unmodulated, Modulated] - a tuple of two boolean values""",
        field="tissue",
    )

    affine_regularization = traits.Enum(
        "mni",
        "eastern",
        "subj",
        "none",
        field="warp.affreg",
        desc="mni, eastern, subj, none ",
    )
    # warping_regularization = traits.Float(field='warp.reg',
    #                                      desc='Aproximate distance between sampling points.')
    warping_regularization = traits.List(traits.Float(), field="warp.reg")
    sampling_distance = traits.Float(
        field="warp.samp", desc="Sampling distance on data for parameter estimation"
    )
    write_deformation_fields = traits.List(
        traits.Bool(),
        minlen=2,
        maxlen=2,
        field="warp.write",
        desc="Which deformation fields to write:[Inverse, Forward]",
    )

    """Elements beyond here are added by Alex to mimic the default SPM12_seg_job.mat file that we were using before."""

    warping_mrf = traits.Bool(field="warp.mrf", desc="MRF modeling in the warp.")
    cleanup = traits.Bool(field="warp.cleanup", desc="Cleanup.")
    warp_fwhm = traits.Float(field="warp.fwhm", desc="FWHM for warp.")


class SPMCommand(base.BaseInterface):
    """Extends `BaseInterface` class to implement SPM specific interfaces.

    WARNING: Pseudo prototype class, meant to be subclassed
    """

    input_spec = spm.base.SPMCommandInputSpec
    _additional_metadata = ["field"]

    _jobtype = "basetype"
    _jobname = "basename"

    _matlab_cmd = None
    _paths = None
    _use_mcr = None

    references_ = [
        {
            "entry": due.BibTeX(
                "@book{FrackowiakFristonFrithDolanMazziotta1997,"
                "author={R.S.J. Frackowiak, K.J. Friston, C.D. Frith, R.J. Dolan, and J.C. Mazziotta},"
                "title={Human Brain Function},"
                "publisher={Academic Press USA},"
                "year={1997},"
                "}"
            ),
            "description": "The fundamental text on Statistical Parametric Mapping (SPM)",
            # 'path': "nipype.interfaces.spm",
            "tags": ["implementation"],
        }
    ]

    def __init__(self, **inputs):
        super(SPMCommand, self).__init__(**inputs)
        self.inputs.on_trait_change(
            self._matlab_cmd_update, ["matlab_cmd", "mfile", "paths", "use_mcr"]
        )
        self._find_mlab_cmd_defaults()
        self._check_mlab_inputs()
        self._matlab_cmd_update()

    @classmethod
    def set_mlab_paths(cls, matlab_cmd=None, paths=None, use_mcr=None):
        cls._matlab_cmd = matlab_cmd
        cls._paths = paths
        cls._use_mcr = use_mcr
        info_dict = spm.base.Info.getinfo(
            matlab_cmd=matlab_cmd, paths=paths, use_mcr=use_mcr
        )

    def _find_mlab_cmd_defaults(self):
        # check if the user has set environment variables to enforce
        # the standalone (MCR) version of SPM
        if self._use_mcr or "FORCE_SPMMCR" in os.environ:
            self._use_mcr = True
            if self._matlab_cmd is None:
                try:
                    self._matlab_cmd = os.environ["SPMMCRCMD"]
                except KeyError:
                    pass

    def _matlab_cmd_update(self):
        # MatlabCommand has to be created here,
        # because matlab_cmd is not a proper input
        # and can be set only during init
        self.mlab = matlab.MatlabCommand(
            matlab_cmd=self.inputs.matlab_cmd,
            mfile=self.inputs.mfile,
            paths=self.inputs.paths,
            resource_monitor=False,
        )
        self.mlab.inputs.script_file = (
            "pyscript_%s.m" % self.__class__.__name__.split(".")[-1].lower()
        )
        if base.isdefined(self.inputs.use_mcr) and self.inputs.use_mcr:
            self.mlab.inputs.nodesktop = Undefined
            self.mlab.inputs.nosplash = Undefined
            self.mlab.inputs.single_comp_thread = Undefined
            self.mlab.inputs.uses_mcr = True
            self.mlab.inputs.mfile = True

    @property
    def version(self):
        info_dict = spm.base.Info.getinfo(
            matlab_cmd=self.inputs.matlab_cmd,
            paths=self.inputs.paths,
            use_mcr=self.inputs.use_mcr,
        )
        if info_dict:
            return "%s.%s" % (info_dict["name"].split("SPM")[-1], info_dict["release"])

    @property
    def jobtype(self):
        return self._jobtype

    @property
    def jobname(self):
        return self._jobname

    def _check_mlab_inputs(self):
        if not base.isdefined(self.inputs.matlab_cmd) and self._matlab_cmd:
            self.inputs.matlab_cmd = self._matlab_cmd
        if not base.isdefined(self.inputs.paths) and self._paths:
            self.inputs.paths = self._paths
        if not base.isdefined(self.inputs.use_mcr) and self._use_mcr:
            self.inputs.use_mcr = self._use_mcr

    def _run_interface(self, runtime):
        """Executes the SPM function using MATLAB."""
        self.mlab.inputs.script = self._make_matlab_command(
            copy.deepcopy(self._parse_inputs())
        )
        results = self.mlab.run()
        runtime.returncode = results.runtime.returncode
        if self.mlab.inputs.uses_mcr:
            if "Skipped" in results.runtime.stdout:
                self.raise_exception(runtime)
        runtime.stdout = results.runtime.stdout
        runtime.stderr = results.runtime.stderr
        runtime.merged = results.runtime.merged
        return runtime

    def _list_outputs(self):
        """Determine the expected outputs based on inputs."""

        raise NotImplementedError

    def _format_arg(self, opt, spec, val):
        """Convert input to appropriate format for SPM."""
        if spec.is_trait_type(traits.Bool):
            return int(val)
        elif spec.is_trait_type(traits.Tuple):
            return list(val)
        else:
            return val

    def _parse_inputs(self, skip=()):
        spmdict = {}
        metadata = dict(field=lambda t: t is not None)
        for name, spec in list(self.inputs.traits(**metadata).items()):
            if skip and name in skip:
                continue
            value = getattr(self.inputs, name)
            if not base.isdefined(value):
                continue
            field = spec.field
            if "." in field:
                fields = field.split(".")
                dictref = spmdict
                for f in fields[:-1]:
                    if f not in list(dictref.keys()):
                        dictref[f] = {}
                    dictref = dictref[f]
                dictref[fields[-1]] = self._format_arg(name, spec, value)
            else:
                spmdict[field] = self._format_arg(name, spec, value)
        return [spmdict]

    def _reformat_dict_for_savemat(self, contents):
        """Encloses a dict representation within hierarchical lists.

        In order to create an appropriate SPM job structure, a Python
        dict storing the job needs to be modified so that each dict
        embedded in dict needs to be enclosed as a list element.

        Examples
        --------
        >>> a = SPMCommand()._reformat_dict_for_savemat(dict(a=1,
        ...                                                  b=dict(c=2, d=3)))
        >>> a == [{'a': 1, 'b': [{'c': 2, 'd': 3}]}]
        True

        """
        newdict = {}
        try:
            for key, value in list(contents.items()):
                if isinstance(value, dict):
                    if value:
                        newdict[key] = self._reformat_dict_for_savemat(value)
                    # if value is None, skip
                else:
                    newdict[key] = value

            return [newdict]
        except TypeError:
            print("Requires dict input")

    def _generate_job(self, prefix="", contents=None):
        """Recursive function to generate spm job specification as a string

        Parameters
        ----------
        prefix : string
            A string that needs to get
        contents : dict
            A non-tuple Python structure containing spm job
            information gets converted to an appropriate sequence of
            matlab commands.

        """
        jobstring = ""
        if contents is None:
            return jobstring
        if isinstance(contents, list):
            for i, value in enumerate(contents):
                if prefix.endswith(")"):
                    newprefix = "%s,%d)" % (prefix[:-1], i + 1)
                else:
                    newprefix = "%s(%d)" % (prefix, i + 1)
                jobstring += self._generate_job(newprefix, value)
            return jobstring
        if isinstance(contents, dict):
            for key, value in list(contents.items()):
                newprefix = "%s.%s" % (prefix, key)
                jobstring += self._generate_job(newprefix, value)
            return jobstring
        if isinstance(contents, np.ndarray):
            if contents.dtype == np.dtype(object):
                if prefix:
                    jobstring += "%s = {...\n" % (prefix)
                else:
                    jobstring += "{...\n"
                for i, val in enumerate(contents):
                    if isinstance(val, np.ndarray):
                        jobstring += self._generate_job(prefix=None, contents=val)
                    elif isinstance(val, list):
                        items_format = []
                        for el in val:
                            items_format += [
                                "{}" if not isinstance(el, (str, bytes)) else "'{}'"
                            ]
                        val_format = ", ".join(items_format).format
                        jobstring += "[{}];...\n".format(val_format(*val))
                    elif isinstance(val, (str, bytes)):
                        jobstring += "'{}';...\n".format(val)
                    else:
                        jobstring += "%s;...\n" % str(val)
                jobstring += "};\n"
            else:
                for i, val in enumerate(contents):
                    for field in val.dtype.fields:
                        if prefix:
                            newprefix = "%s(%d).%s" % (prefix, i + 1, field)
                        else:
                            newprefix = "(%d).%s" % (i + 1, field)
                        jobstring += self._generate_job(newprefix, val[field])
            return jobstring
        if isinstance(contents, (str, bytes)):
            jobstring += "%s = '%s';\n" % (prefix, contents)
            return jobstring
        jobstring += "%s = %s;\n" % (prefix, str(contents))
        return jobstring

    def _make_matlab_command(self, contents, postscript=None):
        """Generates a mfile to build job structure
        Parameters
        ----------

        contents : list
            a list of dicts generated by _parse_inputs
            in each subclass

        cwd : string
            default os.getcwd()

        Returns
        -------
        mscript : string
            contents of a script called by matlab

        """
        cwd = os.getcwd()
        mscript = """
        %% Generated by nipype.interfaces.spm
        if isempty(which('spm')),
             throw(MException('SPMCheck:NotFound', 'SPM not in matlab path'));
        end
        [name, version] = spm('ver');
        fprintf('SPM version: %s Release: %s\\n',name, version);
        fprintf('SPM path: %s\\n', which('spm'));
        spm('Defaults','PET');

        if strcmp(name, 'SPM8') || strcmp(name(1:5), 'SPM12'),
           spm_jobman('initcfg');
           spm_get_defaults('cmdline', 1);
        end\n
        """
        if self.mlab.inputs.mfile:
            if base.isdefined(self.inputs.use_v8struct) and self.inputs.use_v8struct:
                mscript += self._generate_job(
                    "jobs{1}.spm.%s.%s" % (self.jobtype, self.jobname), contents[0]
                )
            else:
                if self.jobname in [
                    "st",
                    "smooth",
                    "preproc",
                    "preproc8",
                    "fmri_spec",
                    "fmri_est",
                    "factorial_design",
                    "defs",
                ]:
                    # parentheses
                    mscript += self._generate_job(
                        "jobs{1}.%s{1}.%s(1)" % (self.jobtype, self.jobname),
                        contents[0],
                    )
                else:
                    # curly brackets
                    mscript += self._generate_job(
                        "jobs{1}.%s{1}.%s{1}" % (self.jobtype, self.jobname),
                        contents[0],
                    )
        else:
            from scipy.io import savemat

            jobdef = {
                "jobs": [
                    {
                        self.jobtype: [
                            {self.jobname: self.reformat_dict_for_savemat(contents[0])}
                        ]
                    }
                ]
            }
            savemat(os.path.join(cwd, "pyjobs_%s.mat" % self.jobname), jobdef)
            mscript += "load pyjobs_%s;\n\n" % self.jobname

        mscript += """\njobs{1}.spm.spatial.preproc.warp.vox = NaN;\n"""
        mscript += """\njobs{1}.spm.spatial.preproc.warp.bb = [NaN NaN NaN
                                                        NaN NaN NaN];"""
        mscript += """
        spm_jobman(\'run\', jobs);\n
        """
        if self.inputs.use_mcr:
            mscript += """
        if strcmp(name, 'SPM8') || strcmp(name(1:5), 'SPM12'),
            close(\'all\', \'force\');
        end;
            """
        if postscript is not None:
            mscript += postscript
        return mscript


class NewSegment(SPMCommand):
    """Use spm_preproc8 (New Segment) to separate structural images into different
    tissue classes. Supports multiple modalities.
    NOTE: This interface currently supports single channel input only
    http://www.fil.ion.ucl.ac.uk/spm/doc/manual.pdf#page=43
    Examples
    --------
    >>> import nipype.interfaces.spm as spm
    >>> seg = spm.NewSegment()
    >>> seg.inputs.channel_files = 'structural.nii'
    >>> seg.inputs.channel_info = (0.0001, 60, (True, True))
    >>> seg.run() # doctest: +SKIP
    For VBM pre-processing [http://www.fil.ion.ucl.ac.uk/~john/misc/VBMclass10.pdf],
    TPM.nii should be replaced by /path/to/spm8/toolbox/Seg/TPM.nii
    >>> seg = NewSegment()
    >>> seg.inputs.channel_files = 'structural.nii'
    >>> tissue1 = (('TPM.nii', 1), 2, (True,True), (False, False))
    >>> tissue2 = (('TPM.nii', 2), 2, (True,True), (False, False))
    >>> tissue3 = (('TPM.nii', 3), 2, (True,False), (False, False))
    >>> tissue4 = (('TPM.nii', 4), 2, (False,False), (False, False))
    >>> tissue5 = (('TPM.nii', 5), 2, (False,False), (False, False))
    >>> seg.inputs.tissues = [tissue1, tissue2, tissue3, tissue4, tissue5]
    >>> seg.run() # doctest: +SKIP
    """

    input_spec = NewSegmentInputSpec
    output_spec = spm.preprocess.NewSegmentOutputSpec

    def __init__(self, **inputs):
        _local_version = spm.SPMCommand().version
        if _local_version and "12." in _local_version:
            self._jobtype = "spatial"
            self._jobname = "preproc"
        else:
            self._jobtype = "tools"
            self._jobname = "preproc8"

        SPMCommand.__init__(self, **inputs)

    def _format_arg(self, opt, spec, val):
        """Convert input to appropriate format for spm"""

        if opt in ["channel_files", "channel_info"]:
            # structure have to be recreated, because of some weird traits error
            new_channel = {}
            new_channel["vols"] = spm.base.scans_for_fnames(self.inputs.channel_files)
            if base.isdefined(self.inputs.channel_info):
                info = self.inputs.channel_info
                new_channel["biasreg"] = info[0]
                new_channel["biasfwhm"] = info[1]
                new_channel["write"] = [int(info[2][0]), int(info[2][1])]
            return [new_channel]
        elif opt == "tissues":
            new_tissues = []
            for tissue in val:
                new_tissue = {}
                new_tissue["tpm"] = np.array(
                    [",".join([tissue[0][0], str(tissue[0][1])])], dtype=object
                )
                new_tissue["ngaus"] = tissue[1]
                new_tissue["native"] = [int(tissue[2][0]), int(tissue[2][1])]
                new_tissue["warped"] = [int(tissue[3][0]), int(tissue[3][1])]
                new_tissues.append(new_tissue)
            return new_tissues
        elif opt == "write_deformation_fields":
            return super(NewSegment, self)._format_arg(
                opt, spec, [int(val[0]), int(val[1])]
            )
        else:
            return super(NewSegment, self)._format_arg(opt, spec, val)

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["native_class_images"] = []
        outputs["dartel_input_images"] = []
        outputs["normalized_class_images"] = []
        outputs["modulated_class_images"] = []
        outputs["transformation_mat"] = []
        outputs["bias_corrected_images"] = []
        outputs["bias_field_images"] = []
        outputs["inverse_deformation_field"] = []
        outputs["forward_deformation_field"] = []

        n_classes = 5
        if base.isdefined(self.inputs.tissues):

            n_classes = len(self.inputs.tissues)
        for i in range(n_classes):
            outputs["native_class_images"].append([])
            outputs["dartel_input_images"].append([])
            outputs["normalized_class_images"].append([])
            outputs["modulated_class_images"].append([])

        for filename in self.inputs.channel_files:
            pth, fbase, ext = utils.filemanip.split_filename(filename)
            if base.isdefined(self.inputs.tissues):
                for i, tissue in enumerate(self.inputs.tissues):
                    if tissue[2][0]:
                        outputs["native_class_images"][i].append(
                            os.path.join(pth, "c%d%s.nii" % (i + 1, fbase))
                        )
                    if tissue[2][1]:
                        outputs["dartel_input_images"][i].append(
                            os.path.join(pth, "rc%d%s.nii" % (i + 1, fbase))
                        )
                    if tissue[3][0]:
                        outputs["normalized_class_images"][i].append(
                            os.path.join(pth, "wc%d%s.nii" % (i + 1, fbase))
                        )
                    if tissue[3][1]:
                        outputs["modulated_class_images"][i].append(
                            os.path.join(pth, "mwc%d%s.nii" % (i + 1, fbase))
                        )
            else:
                for i in range(n_classes):
                    outputs["native_class_images"][i].append(
                        os.path.join(pth, "c%d%s.nii" % (i + 1, fbase))
                    )
            outputs["transformation_mat"].append(
                os.path.join(pth, "%s_seg8.mat" % fbase)
            )

            if base.isdefined(self.inputs.write_deformation_fields):
                if self.inputs.write_deformation_fields[0]:
                    outputs["inverse_deformation_field"].append(
                        os.path.join(pth, "iy_%s.nii" % fbase)
                    )
                if self.inputs.write_deformation_fields[1]:
                    outputs["forward_deformation_field"].append(
                        os.path.join(pth, "y_%s.nii" % fbase)
                    )

            if base.isdefined(self.inputs.channel_info):
                if self.inputs.channel_info[2][0]:
                    outputs["bias_corrected_images"].append(
                        os.path.join(pth, "m%s.nii" % (fbase))
                    )
                if self.inputs.channel_info[2][1]:
                    outputs["bias_field_images"].append(
                        os.path.join(pth, "BiasField_%s.nii" % (fbase))
                    )
        return outputs
