import warnings

warnings.filterwarnings('ignore', r'cmp not installed')
warnings.filterwarnings('ignore', r'This has not been fully tested. Please report any failures.')
warnings.filterwarnings('ignore', r"can't resolve package from __spec__ or __package__")
warnings.simplefilter('ignore', DeprecationWarning)
warnings.simplefilter('ignore', ResourceWarning)


from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
